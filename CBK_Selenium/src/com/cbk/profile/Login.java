package com.cbk.profile;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.cbk.pom.LoginPom;
import com.cbk.projectspec.ProfileFunctions;
import com.cbk.utilgeneric.BaseTest;
import com.cbk.utilgeneric.Data;
import com.cbk.utilgeneric.GetData;
import com.cbk.utilgeneric.TestData;

public class Login extends BaseTest {



@Test(groups="{Regression}",description="124624")
//@Parameters("browserspecfic")
public void TC00_LoginToApplication() throws Exception
{


driver.navigate().to("https://development-eu01-active.demandware.net/on/demandware.store/Sites-dahlie-Site/en_SE/Home-Show");	
	
if (gVar.isMobile()) {
	
	l1.getWebElement(loginObjects.HamburgerMenu).click();
	
}
else {
	
	driver.navigate().to("http://toolsqa.com/automation-practice-table/");	
	int tablecount=l1.getWebElements(loginObjects.Table_webelements).size();
	
	for (int i=0;i<tablecount;i++) {
		
	System.out.println(l1.getWebElements(loginObjects.Table_webelements).get(i).getText());
	
	}
	
}
	


}	
	
	
/*	
@Test(groups="{Regression}",description="124624")
@Parameters("browserspecfic")
public void TC00_LoginToApplication(String browser) throws Exception
{

	log.info("entered in to test case");
	l1.getWebElement(loginObjects.login_link).click();
	log.info("Clicked on login link");
	Thread.sleep(3000);
	p.loginToAppliction(browser,l1);
	log.info("Failure");
	boolean visibility=l1.getWebElement(loginObjects.Login_rewardsHeading).isDisplayed();
	log.info(visibility);
	sa.assertTrue(l1.getWebElement(loginObjects.Login_rewardsHeading).isDisplayed());
	
	
//	log.info("create account button");
//	l1.getWebElement(loginObjects.Login_CreateaccountButton).click();
//	log.info("create account first name enter");
//	l1.getWebElement(registerObjects.Login_CreateaccountFNtextbox).sendKeys("ex@gmail.com");;
//	log.info("clicked on my account button");

}

	
@Test(groups="{Regression}",description="124624")
public void TC00_LoginToApplication() throws Exception
{

	log.info("entered in to test case");	
	driver.findElement(By.xpath("//span[contains(text(),'Login')]")).click();
	log.info("Clicked on login link");
	p.loginToAppliction();
	
	log.info("create account button");
	l1.getWebElement(loginObjects.Login_CreateaccountButton).click();
	log.info("create account first name enter");
	l1.getWebElement(registerObjects.Login_CreateaccountFNtextbox).sendKeys("ex@gmail.com");;
	log.info("clicked on my account button");

}

		

@Test(groups="{Regression}",description="124624")
public void TC00_logIn_Createaccount_() throws Exception
{
	log.info("Click on User account icon");
	l1.getWebElement("MyAccountIcon","Profile\\Register.properties").click();
	log.info("click on login link");
	l1.getWebElement("Login_Icon","Profile\\Register.properties").click();
    log.info("click on create account button");
	l1.getWebElement("Login_CreateAccount", "Profile\\login.properties").click();
	sa.assertTrue(gVar.assertVisible("CreateAccount_Heading", "Profile\\CreateAccount.properties"),"CreateAccount Page Heading");
}
	
	
	@Test(groups="{Regression}",description="288708")
	public void TC01_logIn_CheckOrder_UI_() throws Exception
	{
		
		log.info("Click on User account icon");
		l1.getWebElement("MyAccountIcon","Profile\\Register.properties").click();
		log.info("click on login link");
		l1.getWebElement("Login_Icon","Profile\\Register.properties").click();
		log.info("Check order heading");
		sa.assertTrue(l1.getWebElement("Login_CheckOrder_Heading", "Profile\\login.properties").isDisplayed());
		log.info("Check order status description");
		sa.assertTrue(l1.getWebElement("Login_CheckOrder_Desc", "Profile\\login.properties").isDisplayed());
		log.info("Order Number label");
		sa.assertTrue(l1.getWebElement("Login_OrderNum_Label", "Profile\\login.properties").isDisplayed());
		log.info("Order Number TextBox");
		sa.assertTrue(l1.getWebElement("Login_OrderNum", "Profile\\login.properties").isDisplayed());
		log.info("Order email label");
		sa.assertTrue(l1.getWebElement("Login_OrderEmail_Label", "Profile\\login.properties").isDisplayed());
		log.info("Order email Textbox");
		sa.assertTrue(l1.getWebElement("Login_OrderEmail", "Profile\\login.properties").isDisplayed());
		log.info("Billing Zipcode label");
		sa.assertTrue(l1.getWebElement("Login_OrderPostal_Label", "Profile\\login.properties").isDisplayed());
		log.info("Billing Zipcode textbox");
		sa.assertTrue(l1.getWebElement("Login_OrderPostal", "Profile\\login.properties").isDisplayed());
		log.info("Check Status Button");
		sa.assertTrue(l1.getWebElement("Login_CheckOrder_Button", "Profile\\login.properties").isDisplayed());
		sa.assertAll();
	}
	
	
	@Test(groups="{Regression}",description="288710")
	public void TC02_logIn_RememeberMeCheckboxWithUnChecked_ () throws Exception
	{
		
		log.info("Click on User account icon");
		l1.getWebElement("MyAccountIcon","Profile\\Register.properties").click();
		log.info("click on login link");
		l1.getWebElement("Login_Icon","Profile\\Register.properties").click();
		log.info("Enter the email address");
		l1.getWebElement("Login_Username", "Profile\\login.properties").sendKeys("Vinaylanka@gmail.com");
		
		log.info("Gettting the value of the email textbox");
		String EmailId=l1.getWebElement("Login_Username", "Profile\\login.properties").getText();
		
		log.info("Enter the password");
		l1.getWebElement("Login_Password", "Profile\\login.properties").sendKeys("Vin@74113");
		
		log.info("Gettting the value of the email textbox");
		String Password=l1.getWebElement("Login_Password", "Profile\\login.properties").getText();
		
		log.info("verify the checkbvox status should be unchecked");
		try
		{
			if(l1.getWebElement("Login_Remember_Checkbox", "Profile\\login.properties").isSelected())
			{
				l1.getWebElement("Login_Remember_Checkbox", "Profile\\login.properties").click();
			}
		}
		
		catch(Exception e)
		{
			System.out.println("Remember me checkbox is not checked");
		}
		log.info("click on signin button");
		l1.getWebElement("Login_Login_Button", "Profile\\login.properties").click();
		log.info("Myaccount section page verificatrion");
		sa.assertTrue(l1.getWebElement("MyAccount_options","Profile\\MyAccountHome.properties").isDisplayed());
		log.info("Click on logout button");
		l1.getWebElement("MyAccount_LogoutLink", "Profile\\MyAccountHome.properties").click();
		
		log.info("Empty email textbox verification");
		
		
		log.info("Gettting the value of the email textbox");
		String EmptyEmailId=l1.getWebElement("Login_Username", "Profile\\login.properties").getText();
		
		log.info("Gettting the value of the email textbox");
		String EmptyPassword=l1.getWebElement("Login_Password", "Profile\\login.properties").getText();
		log.info("Email id textbox verification");
		sa.assertNotEquals(EmailId, EmptyEmailId);
		
		log.info("Email id textbox verification");
		sa.assertNotEquals(Password, EmptyPassword);
		try
		{
			if(!l1.getWebElement("Login_Remember_Checkbox", "Profile\\login.properties").isSelected())
			{
				System.out.println("Remeber me checkbox ix not checked");
			}
		}
	
		catch(Exception e)
		{
			
		}
	}
	
	
	@Test(groups="{Regression}",description="124615")
	public void TC03_logIn_UI_() throws Exception
	{
		p.loginToAppliction();
		
		log.info("Myaccount section page verificatrion");
		sa.assertTrue(l1.getWebElement("MyAccount_options","Profile\\MyAccountHome.properties").isDisplayed());
		log.info("Click on logout button");
		l1.getWebElement("MyAccount_LogoutLink", "Profile\\MyAccountHome.properties").click();
		
	}
	
	@Test(groups="{Regression}",description="148631")
	
	public void TC04_logIn_ForgotPassword() throws Exception
	{
		log.info("Click on forgotpassword link");
		l1.getWebElement("Login_ForgotPasswrd", "Profile\\login.properties").click();
		log.info("Forgot password dialog");
        sa.assertTrue(l1.getWebElement("ForgotPassword_Dailog", "Profile\\ForgotPassword.properties").isDisplayed());
        log.info("Forgot password dialog Text Heading");
        sa.assertTrue(l1.getWebElement("ForgotPassword_DialogText", "Profile\\ForgotPassword.properties").isDisplayed());
        log.info("closing Forgot password dialog ");
        sa.assertTrue(l1.getWebElement("ForgotPassword_CloseButton", "Profile\\ForgotPassword.properties").isDisplayed());
        log.info("Closing the forgot password overlay");
        l1.getWebElement("ForgotPassword_CloseButton", "Profile\\ForgotPassword.properties").click();
        
	}
	
	
	@Test(groups="{Regression}",description="124812")
	public void TC05_logIn_CheckOrderStatus() throws Exception
	{
	
		log.info("Enter Order id in Order number textbox");
		l1.getWebElement("Login_OrderNum", "Profile\\login.properties").sendKeys("PRV00077205");
		log.info("Entering ordered email id");
		l1.getWebElement("Login_OrderEmail", "Profile\\login.properties").sendKeys("Vinaylanka@gmail.com");
		log.info("Entering ZipCode");
		l1.getWebElement("Login_OrderPostal", "Profile\\login.properties").sendKeys("60707");
		log.info("Click on check order status button");
		l1.getWebElement("Login_CheckOrder_Button", "Profile\\login.properties").click();
		log.info("Verification of order details page of respective order");
		String ExpectedOrderId=l1.getWebElement("CheckOrderStatus_OrderHistory_OrderId", "Profile\\OrderHistory.properties").getText();
		sa.assertEquals("PRV00077205", ExpectedOrderId);
        log.info("Order Number Heading");
		sa.assertTrue(l1.getWebElement("CheckOrderStatus_OrderHistory_OrderNumberHeading", "Profile\\OrderHistory.properties").isDisplayed());
		log.info("Breadcrumb verification");
		log.info("MyAccount text in the breadcrumb");	
		sa.assertEquals("My Account", l1.getWebElements("OrderHistory_Breadcrumb_Elements", "Profile\\OrderHistory.properties").get(0).getText());
		log.info("Order History text in the breadcrumb");	
		sa.assertEquals("Order History", l1.getWebElements("OrderHistory_Breadcrumb_Elements", "Profile\\OrderHistory.properties").get(1).getText());
		log.info("Order History id in the breadcrumb");	
		sa.assertEquals("PRV00077205", l1.getWebElements("OrderHistory_Breadcrumb_Elements", "Profile\\OrderHistory.properties").get(2).getText());
	}
	
	
	
	@Test(groups="{Regression}",description="215432")
	public void TC06_logIn_RememeberMeCheckboxWithChecked_ () throws Exception
	{
		
		log.info("Click on User account icon");
		l1.getWebElement("MyAccountIcon","Profile\\Register.properties").click();
		log.info("click on login link");
		l1.getWebElement("Login_Icon","Profile\\Register.properties").click();
		
		log.info("Enter the email address");
		l1.getWebElement("Login_Username", "Profile\\login.properties").sendKeys("Vinaylanka@gmail.com");
		
		log.info("Gettting the value of the email textbox");
		String EmailId=l1.getWebElement("Login_Username", "Profile\\login.properties").getText();
		
		log.info("Enter the password");
		l1.getWebElement("Login_Password", "Profile\\login.properties").sendKeys("Vin@74113");
		
		log.info("Gettting the value of the email textbox");
		String Password=l1.getWebElement("Login_Password", "Profile\\login.properties").getText();
		
		log.info("verify the checkbvox status should be unchecked");
		try
		{
			if(!l1.getWebElement("Login_Remember_Checkbox", "Profile\\login.properties").isSelected())
			{
				l1.getWebElement("Login_Remember_Checkbox", "Profile\\login.properties").click();
			}
		}
		
		catch(Exception e)
		{
			System.out.println("Remember me checkbox is  checked");
		}
		log.info("click on signin button");
		l1.getWebElement("Login_Login_Button", "Profile\\login.properties").click();
		log.info("Myaccount section page verificatrion");
		sa.assertTrue(l1.getWebElement("MyAccount_options","Profile\\MyAccountHome.properties").isDisplayed());
		 log.info("Click on User account icon");
     	l1.getWebElement("MyAccountIcon","Profile\\Register.properties").click();
		log.info("Click on logout button");
		l1.getWebElement("MyAccount_Logout", "Profile\\MyAccountHome.properties").click();
		
		log.info("Email Id Verification");
		sa.assertEquals(EmailId, l1.getWebElement("Login_Username", "Profile\\login.properties").getText());
		
		log.info("remember me checkbox verification");
		sa.assertTrue(l1.getWebElement("Login_Remember_Checkbox", "Profile\\login.properties").isSelected());
	}
	
	
	@Test(groups="{Regression}",description="215432")
	public void TC07_logIn_ReadMoreSecurityLink_() throws Exception
	{
		
		log.info("click on read more securiy link");
		l1.getWebElement("Login_ReadmoreAbtSecurity", "Profile\\login.properties").click();
		log.info("Security page verification");
		l1.getWebElement("SecurityPage_Heading", "Profile\\login.properties").click();
		
	}

	@Test(groups="{Regression}",description="124606")
	public void TC08_logIn_NewCustomer_UI() throws Exception
	{
		
		
		log.info("Click on User account icon");
		l1.getWebElement("MyAccountIcon","Profile\\Register.properties").click();
		log.info("click on login link");
		l1.getWebElement("MyAccount_Logout","Profile\\MyAccountHome.properties").click();
		
		
		log.info("Enter the password");
		l1.getWebElement("Login_Password", "Profile\\login.properties").sendKeys("Vin@74113");
		log.info("verify the checkbvox status should be unchecked");
		try
		{
			if(l1.getWebElement("Login_Remember_Checkbox", "Profile\\login.properties").isSelected())
			{
				l1.getWebElement("Login_Remember_Checkbox", "Profile\\login.properties").click();
			}
		}
		
		catch(Exception e)
		{
			System.out.println("Remember me checkbox is not checked");
		}
		log.info("click on signin button");
		l1.getWebElement("Login_Login_Button", "Profile\\login.properties").click();
		
		log.info("Click on User account icon");
		l1.getWebElement("MyAccountIcon","Profile\\Register.properties").click();
		log.info("click on login link");
		l1.getWebElement("MyAccount_Logout","Profile\\MyAccountHome.properties").click();
		
		log.info("New Customer text");
		sa.assertTrue(l1.getWebElement("Login_NewCustomer_Heading","Profile\\login.properties").isDisplayed());
		log.info("New Customer Header discription");
		sa.assertTrue(l1.getWebElement("Login_CheckOrder_Desc","Profile\\login.properties").isDisplayed());
		log.info("Create Account button");
		sa.assertTrue(l1.getWebElement("Login_CreateAccount","Profile\\login.properties").isDisplayed());
		log.info("Benifits of create account discription");
		sa.assertTrue(l1.getWebElement("Login_CreateAccount_Benefits","Profile\\login.properties").isDisplayed());
		log.info("Content asset");
		sa.assertTrue(l1.getWebElement("Login_CreateAccount_Benefit_ContentAsset","Profile\\login.properties").isDisplayed());
		log.info("Newandexclusiveoffter");
		sa.assertTrue(l1.getWebElement("Login_Newandexclusiveoffter_text","Profile\\login.properties").isDisplayed());
		log.info("OrderHistory");
		sa.assertTrue(l1.getWebElement("Login_OrderHistory_text","Profile\\login.properties").isDisplayed());
		log.info("Faster Checkout");
		sa.assertTrue(l1.getWebElement("Login_FasterCheckout_text","Profile\\login.properties").isDisplayed());
		log.info("Read more about text");
		sa.assertTrue(l1.getWebElement("Login_ReadmoreAbtSecurity","Profile\\login.properties").isDisplayed());
		
	}
	
	@Test(groups="{Regression}",description="148627_124604_124605_148632")
	public void TC09_logIn_LoaginUI() throws Exception
	{
		log.info("Breadcrumb");
		sa.assertTrue(l1.getWebElement("Login_Breadcrumb","Profile\\login.properties").isDisplayed());
		log.info("My account login' as page title");
		sa.assertTrue(l1.getWebElement("Login_Heading","Profile\\login.properties").isDisplayed());
		log.info("Returning customer");
		sa.assertTrue(l1.getWebElement("Login_ReturningCustomer_Heading","Profile\\login.properties").isDisplayed());
		log.info("Login static text");
		sa.assertTrue(l1.getWebElement("Login_Static_Pragraph","Profile\\login.properties").isDisplayed());
		log.info("Email Label");
		sa.assertTrue(l1.getWebElement("Login_UN_Label","Profile\\login.properties").isDisplayed());
		log.info("Email Textbox");
		sa.assertTrue(l1.getWebElement("Login_Username","Profile\\login.properties").isDisplayed());
		log.info("Password label");
		sa.assertTrue(l1.getWebElement("Login_Pwd_Label","Profile\\login.properties").isDisplayed());
		log.info("Email Textbox");
		sa.assertTrue(l1.getWebElement("Login_Password","Profile\\login.properties").isDisplayed());
		log.info("Login button");
		sa.assertTrue(l1.getWebElement("Login_Login_Button","Profile\\login.properties").isDisplayed());
		log.info("Remember me checkbox");
		sa.assertTrue(l1.getWebElement("Login_Remember_Checkbox","Profile\\login.properties").isDisplayed());
		log.info("Remember me label");
		sa.assertTrue(l1.getWebElement("Login_Remember_Label","Profile\\login.properties").isDisplayed());
		log.info("Forgot password label");
		sa.assertTrue(l1.getWebElement("Login_ForgotPasswrd","Profile\\login.properties").isDisplayed());
		log.info("");
		sa.assertTrue(l1.getWebElement("Login_ORtext","Profile\\login.properties").isDisplayed());
		log.info("");
		sa.assertTrue(l1.getWebElement("Login_LoginSuggestion","Profile\\login.properties").isDisplayed());
		log.info("");
		sa.assertTrue(l1.getWebElement("Login_GooglePlus_Icon","Profile\\login.properties").isDisplayed());
		log.info("");
		sa.assertTrue(l1.getWebElement("Login_LinkedIn_Icon","Profile\\login.properties").isDisplayed());
		log.info("");
		sa.assertTrue(l1.getWebElement("Login_Microsoft_Icon","Profile\\login.properties").isDisplayed());
		log.info("");
		sa.assertTrue(l1.getWebElement("Login_Facebook_Icon","Profile\\login.properties").isDisplayed());
		log.info("");
		sa.assertTrue(l1.getWebElement("Login_GitHub_Icon","Profile\\login.properties").isDisplayed());
		log.info("");
		sa.assertTrue(l1.getWebElement("Login_SinaWeibo_Icon","Profile\\login.properties").isDisplayed());
		log.info("");
		sa.assertTrue(l1.getWebElement("Login_VKontakte_Icon","Profile\\login.properties").isDisplayed());
		log.info("");
		sa.assertTrue(l1.getWebElement("Login_CheckOrder_Heading","Profile\\login.properties").isDisplayed());
		log.info("");
		sa.assertTrue(l1.getWebElement("Login_CheckOrder_Desc","Profile\\login.properties").isDisplayed());
		log.info("");
		sa.assertTrue(l1.getWebElement("Login_OrderNum_Label","Profile\\login.properties").isDisplayed());
		log.info("");
		sa.assertTrue(l1.getWebElement("Login_OrderNum","Profile\\login.properties").isDisplayed());
		log.info("");
		sa.assertTrue(l1.getWebElement("Login_OrderEmail_Label","Profile\\login.properties").isDisplayed());
		log.info("");
		sa.assertTrue(l1.getWebElement("Login_OrderEmail","Profile\\login.properties").isDisplayed());
		log.info("");
		sa.assertTrue(l1.getWebElement("Login_OrderPostal_Label","Profile\\login.properties").isDisplayed());
		log.info("");
		sa.assertTrue(l1.getWebElement("Login_OrderPostal","Profile\\login.properties").isDisplayed());
		log.info("");
		sa.assertTrue(l1.getWebElement("Login_CheckOrder_Button","Profile\\login.properties").isDisplayed());
		log.info("");
		sa.assertTrue(l1.getWebElement("Login_OrderNum_Label","Profile\\login.properties").isDisplayed());
		log.info("New Customer text");
		sa.assertTrue(l1.getWebElement("Login_NewCustomer_Heading","Profile\\login.properties").isDisplayed());
		log.info("New Customer Header discription");
		sa.assertTrue(l1.getWebElement("Login_CheckOrder_Desc","Profile\\login.properties").isDisplayed());
		log.info("Create Account button");
		sa.assertTrue(l1.getWebElement("Login_CreateAccount","Profile\\login.properties").isDisplayed());
		log.info("Benifits of create account discription");
		sa.assertTrue(l1.getWebElement("Login_CreateAccount_Benefits","Profile\\login.properties").isDisplayed());
		log.info("Content asset");
		sa.assertTrue(l1.getWebElement("Login_CreateAccount_Benefit_ContentAsset","Profile\\login.properties").isDisplayed());
		log.info("Newandexclusiveoffter");
		sa.assertTrue(l1.getWebElement("Login_Newandexclusiveoffter_text","Profile\\login.properties").isDisplayed());
		log.info("OrderHistory");
		sa.assertTrue(l1.getWebElement("Login_OrderHistory_text","Profile\\login.properties").isDisplayed());
		log.info("Faster Checkout");
		sa.assertTrue(l1.getWebElement("Login_FasterCheckout_text","Profile\\login.properties").isDisplayed());
		log.info("Read more about text");
		sa.assertTrue(l1.getWebElement("Login_ReadmoreAbtSecurity","Profile\\login.properties").isDisplayed());
		log.info("LeftNavigation Headings");
		int LeftnavHeadings=l1.getWebElements("LeftNav_Headings", "Profile\\login.properties").size();
		for(i=0; i<LeftnavHeadings; i++)
		{
			sa.assertTrue(l1.getWebElements("LeftNav_Headings", "Profile\\login.properties").get(i).isDisplayed());
		}
		log.info("LeftNavigation Links");
		int LeftNavLinks=l1.getWebElements("LeftNav_Links", "Profile\\login.properties").size();
		for(i=0; i<LeftNavLinks; i++)
		{
			sa.assertTrue(l1.getWebElements("LeftNav_Links", "Profile\\login.properties").get(i).isDisplayed());
		}
		log.info("Need help text");
		sa.assertTrue(l1.getWebElement("LeftNav_NeedHelpTxt","Profile\\login.properties").isDisplayed());
		log.info("Contact us link text");
		sa.assertTrue(l1.getWebElement("LeftNav_ContactUs","Profile\\login.properties").isDisplayed());
		int LeftnavNeedhelpCotent=l1.getWebElements("LeftNav_NeedhelpContentAsset", "Profile\\login.properties").size();
		for(i=0; i<LeftnavNeedhelpCotent; i++)
		{
			 sa.assertTrue(l1.getWebElements("LeftNav_NeedhelpContentAsset", "Profile\\login.properties").get(i).isDisplayed());		
	    }
	    sa.assertAll();
	}

	
	@Test(groups="{Regression}",description="148628")

		public void TC10logIn_LeftNavLinksNavigation_() throws Exception
		{
			
			log.info("Click on LeftNavigation links");
			int LeftNavLinks=l1.getWebElements("LeftNav_Links", "Profile\\login.properties").size();
			for(i=0; i<=LeftNavLinks; i++)
			{
				if(i==0)
				{
				l1.getWebElements("LeftNav_Links", "Profile\\login.properties").get(i).click();
                    log.info("Create account page verification");
                    sa.assertTrue(l1.getWebElement("CreateAccount_Heading", "Profile\\CreateAccount.properties").isDisplayed());
                    l1.getWebElement("MyAccountIcon","Profile\\Register.properties").click();
                	l1.getWebElement("Login_Icon","Profile\\Register.properties").click();
				}
				else if(i==1)
				{
					l1.getWebElements("LeftNav_Links", "Profile\\login.properties").get(i).click();
					  log.info("Create account page verification");
	                    sa.assertTrue(l1.getWebElement("PrivacyPolich_Heading", "Profile\\login.properties").isDisplayed());
	                    l1.getWebElement("MyAccountIcon","Profile\\Register.properties").click();
	                	l1.getWebElement("Login_Icon","Profile\\Register.properties").click();
				}
				
				else if(i==2)
				{
					l1.getWebElement("MyAccountIcon","Profile\\Register.properties").click();
					l1.getWebElement("Login_Icon","Profile\\Register.properties").click();
					l1.getWebElements("LeftNav_Links", "Profile\\login.properties").get(i).click();
					  log.info("Create account page verification");
	                    sa.assertTrue(l1.getWebElement("SecurityPage_Heading", "Profile\\login.properties").isDisplayed());
				}
				
			}
			driver.navigate().back();
			log.info("Click on contact us link");
			l1.getWebElement("LeftNav_ContactUs", "Profile\\login.properties").click();
			log.info("Contact Us page verification");
            sa.assertTrue(l1.getWebElement("ContactUs_Heading", "Profile\\login.properties").isDisplayed());
            l1.getWebElement("MyAccountIcon","Profile\\Register.properties").click();
			l1.getWebElement("Login_Icon","Profile\\Register.properties").click();

		}
	
		
		@Test(groups="{Regression}",description="124610",dataProvider="LoginEmailValidation",dataProviderClass=Data.class)
		public void TC02_logIn_EmailIdvalidation(TestData L) throws Exception
		{
			p.ClearEmailId();
			log.info("fetching data from data providers");
			l1.getWebElement("Login_Username", "Profile\\login.properties").sendKeys(L.get(0));
			l1.getWebElement("Login_Password", "Profile\\login.properties").sendKeys("Vin@74113");
			log.info("click on login button");
			l1.getWebElement("Login_Login_Button", "Profile\\login.properties").click();
		
			
			
			if(i==0 || i==1 || i==2)
			{
				
				//Error Messages
				sa.assertEquals(l1.getWebElement("Login_Username_ErrorMsg", "Profile\\login.properties").getText(),GetData.getDataFromProperties("\\POM\\ErrorMessages\\ErrorMessages.properties","EmailId_Errormessage"));
				
			}
		
			else if(i==3)
			{
				//Error Messages
				sa.assertEquals(l1.getWebElement("Login_Username_ErrorMsg", "Profile\\login.properties").getText(),GetData.getDataFromProperties("\\POM\\ErrorMessages\\ErrorMessages.properties","EmailId_RequiredErrorMessage"));
			}
			
		
			else if(i==4 || i==5 || i==6 || i==7 || i==8)
				{
				//Error Messages
				sa.assertEquals(GetData.getDataFromProperties("\\POM\\ErrorMessages\\ErrorMessages.properties","EmailId_Errormessage"),l1.getWebElement("Login_Username_ErrorMsg", "Profile\\login.properties").getText());
			}
			
			else if(i==9 || i==10)
			{
				log.info("length Validation"+i);
				sa.assertEquals(GetData.getDataFromExcel("\\data\\ProfileData.xls","EmailValidation",1,1),l1.getWebElement("Login_Username", "Profile\\login.properties").getText().length());
				//Error Messages
				sa.assertEquals(l1.getWebElement("Login_Username_ErrorMsg", "Profile\\login.properties").getText(),GetData.getDataFromProperties("\\POM\\ErrorMessages\\ErrorMessages.properties","EmailId_Errormessage"));
			}
			
			else
			{
				log.info("Myaccount section page verificatrion");
				sa.assertTrue(l1.getWebElement("MyAccount_options","Profile\\MyAccountHome.properties").isDisplayed());
			}
		
			sa.assertAll();
		}
			
		
		@Test(groups="{Regression}",description="288709",dataProvider="OrderEmailIdvalidation",dataProviderClass=Data.class)
		public void TC12_logIn_OrderEmailIdvalidation(TestData S) throws Exception
		{
			log.info("fetching data from data providers");
			l1.getWebElement("Login_OrderEmail", "Profile\\login.properties").clear();
			l1.getWebElement("Login_OrderNum", "Profile\\login.properties").clear();
			l1.getWebElement("Login_OrderPostal", "Profile\\login.properties").clear();

			
			
			l1.getWebElement("Login_OrderNum", "Profile\\login.properties").sendKeys("PRV00077205");
			l1.getWebElement("Login_OrderEmail", "Profile\\login.properties").sendKeys(S.get(0));
			l1.getWebElement("Login_OrderPostal", "Profile\\login.properties").sendKeys("60707");
			log.info("click on Checkorder button");
			l1.getWebElement("Login_CheckOrder_Button", "Profile\\login.properties").click();
			
			System.out.println(i);
			if(i==0 || i==1 || i==2)
			{
			sa.assertEquals(GetData.getDataFromExcel("\\data\\ProfileData.xls","Emailvalidation",1,0),l1.getWebElement("Login_OrderEmail", "Profile\\login.properties").getCssValue("background-color"));
				//Error Messages
				sa.assertEquals(GetData.getDataFromProperties("\\POM\\ErrorMessages\\ErrorMessages.properties","EmailId_Errormessage"),l1.getWebElement("Checkorder_EmailId_Errormessage", "Profile\\login.properties").getText());
				
			}
			
			else if(i==3)
			{
				//sa.assertEquals(GetData.getDataFromExcel("\\data\\ProfileData.xls","Emailvalidation",1,0),l1.getWebElement("Login_OrderEmail", "Profile\\login.properties").getCssValue("background-color"));
				//Error Messages
				sa.assertEquals(GetData.getDataFromProperties("\\POM\\ErrorMessages\\ErrorMessages.properties","EmailId_RequiredErrorMessage"),l1.getWebElement("Checkorder_EmailId_Errormessage", "Profile\\login.properties").getText());
				
			}
			
			if(i==4 || i==5 || i==6 || i==7 || i==8)
			{
				//sa.assertEquals(GetData.getDataFromExcel("\\data\\ProfileData.xls","Emailvalidation",1,0),l1.getWebElement("Login_OrderEmail", "Profile\\login.properties").getCssValue("background-color"));
				//Error Messages
				sa.assertEquals(GetData.getDataFromProperties("\\POM\\ErrorMessages\\ErrorMessages.properties","EmailId_Errormessage"),l1.getWebElement("Checkorder_EmailId_Errormessage", "Profile\\login.properties").getText());
				
			}
			
			else if(i==9 || i==10)
			{
				System.out.println("Comint to 9th iteration");
				log.info("length Validation"+i);
				sa.assertEquals(GetData.getDataFromExcel("\\data\\ProfileData.xls","Emailvalidation",1,1),l1.getWebElement("Login_OrderEmail", "Profile\\login.properties").getText().length());
				//Error Messages
				sa.assertEquals(GetData.getDataFromProperties("\\POM\\ErrorMessages\\ErrorMessages.properties","EmailId_InvlaidEmailIdErrorMessage"),l1.getWebElement("Login_InvalidCredentials_ErrorMsg", "Profile\\login.properties").getText());
			}
			
			else if(i==11)
			{
				Thread.sleep(1000);
				log.info("Order Number Heading");
				sa.assertTrue(l1.getWebElement("CheckOrderStatus_OrderHistory_OrderNumberHeading", "Profile\\OrderHistory.properties").isDisplayed());
			}
			i++;
			sa.assertAll();
			
		}
		
		
		@Test(groups="{Regression}",description="124627",dataProvider="OrderIdvalidation",dataProviderClass=Data.class)
		public void TC13_logIn_Ordernumbervalidation(TestData t) throws Exception
		{
			
			

			log.info("fetching data from data providers");
		
			l1.getWebElement("Login_OrderEmail", "Profile\\login.properties").clear();
			l1.getWebElement("Login_OrderNum", "Profile\\login.properties").clear();
			l1.getWebElement("Login_OrderPostal", "Profile\\login.properties").clear();
			
			l1.getWebElement("Login_OrderNum", "Profile\\login.properties").sendKeys(t.get(0));
			l1.getWebElement("Login_OrderEmail", "Profile\\login.properties").sendKeys("Vinaylanka@gmail.com");
			l1.getWebElement("Login_OrderPostal", "Profile\\login.properties").sendKeys("60707");
			log.info("click on Checkorder button");
			l1.getWebElement("Login_CheckOrder_Button", "Profile\\login.properties").click();
			l1.getWebElement("Login_OrderNum", "Profile\\login.properties").clear();
			System.out.println(i);

			if(i==0)
			{
				Thread.sleep(1000);
				//Error Messages
				sa.assertEquals(GetData.getDataFromProperties("\\POM\\ErrorMessages\\ErrorMessages.properties","EmailId_InvlaidEmailIdErrorMessage"),l1.getWebElement("Login_InvalidCredentials_ErrorMsg", "Profile\\login.properties").getText());
				
			}
			
			else if(i==1)
			{
				Thread.sleep(1000);
				//sa.assertEquals(GetData.getDataFromExcel("\\data\\ProfileData.xls","OrderIdValidation",1,0),l1.getWebElement("Login_OrderNum", "Profile\\login.properties").getCssValue("background-color"));
				//Error Messages
				sa.assertEquals(GetData.getDataFromProperties("\\POM\\ErrorMessages\\ErrorMessages.properties","OrderId_EmptyErrormessage"),l1.getWebElement("CheckOrder_OrderId_ErrorMessage", "Profile\\login.properties").getText());
				
			}
			
			else if(i==2||i==3)
			{
				Thread.sleep(1000);
				sa.assertEquals(GetData.getDataFromExcel("\\data\\ProfileData.xls","OrderIdValidation",1,1),l1.getWebElement("Login_OrderNum", "Profile\\login.properties").getText().length());
				int lengthvalue=l1.getWebElement("Login_OrderNum", "Profile\\login.properties").getText().length();
				System.out.println(lengthvalue);
				//Error Messages
				sa.assertEquals(GetData.getDataFromProperties("\\POM\\ErrorMessages\\ErrorMessages.properties","EmailId_InvlaidEmailIdErrorMessage"),l1.getWebElement("Login_InvalidCredentials_ErrorMsg", "Profile\\login.properties").getText());
			}
			
			else if(i==4)
			{
				Thread.sleep(1000);
				log.info("Order Number Heading");
				sa.assertTrue(l1.getWebElement("CheckOrderStatus_OrderHistory_OrderNumberHeading", "Profile\\OrderHistory.properties").isDisplayed());
				
			}
		i++;
			sa.assertAll();
		}
			
		
		@Test(groups="{Regression}",description="148636",dataProvider="LoginEmailValidation",dataProviderClass=Data.class)
		public void TC14_logIn_Passwordvalidation(TestData l) throws Exception
		{
			
			log.info("fetching data from data providers");
			l1.getWebElement("Login_Username", "Profile\\login.properties").sendKeys("Vinaylanka@gmail.com");
			l1.getWebElement("Login_Password", "Profile\\login.properties").sendKeys(l.get(0));
			log.info("click on login button");
			l1.getWebElement("Login_Login_Button", "Profile\\login.properties").click();
			l1.getWebElement("Login_Password", "Profile\\login.properties").clear();
			
			if(i==0 || i==1)
			{
				sa.assertEquals(GetData.getDataFromExcel("\\data\\ProfileData.xls","Passwordvalidation",1,0),l1.getWebElement("Login_Username", "Profile\\login.properties").getCssValue("background-color"));
				//Error Messages
				sa.assertEquals(GetData.getDataFromProperties("POM\\ErrorMessages\\ErrorMessages.properties","EmailId_Errormessage"),l1.getWebElement("Login_Password", "Profile\\login.properties").getText());
				
			}
			
			if(i==2)
			{
				//Error Messages
				sa.assertEquals(GetData.getDataFromProperties("POM\\ErrorMessages\\ErrorMessages.properties","Password_EmptyErrormessage"),l1.getWebElement("Login_Password", "Profile\\login.properties").getText());
			}
			
			if(i==3)
			{
				sa.assertEquals(GetData.getDataFromExcel("\\data\\ProfileData.xls","OrderIdValidation",1,1),l1.getWebElement("Login_Password", "Profile\\login.properties").getText().length());
			}
			else
			{
				log.info("Myaccount section page verificatrion");
				sa.assertTrue(l1.getWebElement("MyAccount_options","Profile\\MyAccountHome.properties").isDisplayed());
			}
		}
	
	
		@Test(groups="{Regression}",description="148637",dataProvider="LoginEmailValidation",dataProviderClass=Data.class)
		public void TC15_logIn_ZipCodedvalidation_ (TestData l) throws Exception
		{
			
			log.info("fetching data from data providers");
			
			l1.getWebElement("Login_OrderNum", "Profile\\login.properties").sendKeys("PRV00077205");
			l1.getWebElement("Login_OrderEmail", "Profile\\login.properties").sendKeys("Vinaylanka@gmail.com");
			l1.getWebElement("Login_OrderPostal", "Profile\\login.properties").sendKeys(l.get(0));
			log.info("click on Checkorder button");
			l1.getWebElement("Login_CheckOrder_Button", "Profile\\login.properties").click();
			l1.getWebElement("Login_OrderPostal", "Profile\\login.properties").clear();
			
			
			if(i==0 || i==1 || i==2 || i==3 ||i==4)
			{
				//Error Messages
				sa.assertEquals(GetData.getDataFromProperties("POM\\ErrorMessages\\ErrorMessages.properties","InvalidZipcode_errormessage"),l1.getWebElement("Login_OrderPostal", "Profile\\login.properties").getText());
				
			}
			
			else if(i==5 || i==6)
			{
				//Error Messages
				sa.assertEquals(GetData.getDataFromProperties("POM\\ErrorMessages\\ErrorMessages.properties","ZipCode_EmptyErrormessage"),l1.getWebElement("Login_OrderPostal", "Profile\\login.properties").getText());
			}
			
			
			else if(i==7)
			{
				//Error Messages
				sa.assertEquals(GetData.getDataFromProperties("POM\\ErrorMessages\\ErrorMessages.properties","ZipCode_Errormessage"),l1.getWebElement("Login_OrderPostal", "Profile\\login.properties").getText());
			}
			else
			{
				log.info("Order Number Heading");
				sa.assertTrue(l1.getWebElement("CheckOrderStatus_OrderHistory_OrderNumberHeading", "Profile\\OrderHistory.properties").isDisplayed());
			}
			
		}*/
	
			
}


