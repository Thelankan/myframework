package com.cbk.profile;


import java.util.ArrayList;
import java.util.List;












import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.cbk.projectspec.ProfileFunctions;
import com.cbk.utilgeneric.BaseTest;
import com.cbk.utilgeneric.Data;
import com.cbk.utilgeneric.GetData;
import com.cbk.utilgeneric.TestData;

public class PaymentSettings extends BaseTest{
	
	//125897,128535,128723
	
 @Test(groups="{Regression}",description="124782,125897")
public void TC00_PaymentSettingsUI() throws Exception
{
	 
p.loginToAppliction(l1);
log.info("Click on Payment method link");
l1.getWebElement("PaymentSettings_Link_MyaccountLandingPage","Profile\\PaymentSettings.properties").click();
log.info("Verify the UI of Payment setting page");
sa.assertTrue(gVar.assertVisible("PaymentSettings_Heading","Profile\\PaymentSettings.properties"),"Payment settings heading");
sa.assertTrue(gVar.assertVisible("PaymentSettings_AddCreditCard_Button","Profile\\PaymentSettings.properties"),"Add credit card button");
sa.assertTrue(gVar.assertVisible("PaymentSettings_LeftNav","Profile\\PaymentSettings.properties"),"Payment settings LeftNav");
sa.assertTrue(gVar.assertVisible("PaymentSettings_breadcrumb","Profile\\PaymentSettings.properties"),"Payment settings breadcrumb");
Thread.sleep(3000);
log.info("Delete credit card");
p.DeleteCreditCard();
log.info("Click on add credit card button");
Thread.sleep(3000);
l1.getWebElement("PaymentSettings_AddCreditCard_Button","Profile\\PaymentSettings.properties").click();
log.info("select Credit card button ");
l1.getWebElement("PaymentSetting_Type_CC","Profile\\PaymentSettings.properties").click();
Select dropdown=new Select(l1.getWebElement("PaymentSetting_ListOfCC","Profile\\PaymentSettings.properties"));
dropdown.selectByIndex(1);
log.info("Add credit card UI");
sa.assertTrue(l1.getWebElement("PaymentSettings_AddCreditCardOverlay_Heading", "Profile\\PaymentSettings.properties").isDisplayed(),"Credit Card Overlay Heading");
sa.assertTrue(l1.getWebElement("PaymentSettings_NameOnCard", "Profile\\PaymentSettings.properties").isDisplayed(),"Credit Card Name on card");
sa.assertTrue(l1.getWebElement("PaymentSettings_type", "Profile\\PaymentSettings.properties").isDisplayed(),"Credit Card payment type");
sa.assertTrue(l1.getWebElement("PaymentSettings_Number", "Profile\\PaymentSettings.properties").isDisplayed(),"Credit Card payment NUmber");
sa.assertTrue(l1.getWebElement("PaymentSettings_ExpiresMonth", "Profile\\PaymentSettings.properties").isDisplayed(),"Credit Card Expires month");
sa.assertTrue(l1.getWebElement("PaymentSettings_ExpiresYear", "Profile\\PaymentSettings.properties").isDisplayed(),"Credit Card Expires Year");
sa.assertTrue(l1.getWebElement("PaymentSettings_ApplyButton", "Profile\\PaymentSettings.properties").isDisplayed());
sa.assertTrue(l1.getWebElement("PaymentSettings_CancelButton", "Profile\\PaymentSettings.properties").isDisplayed());
sa.assertTrue(l1.getWebElement("PaymentSettings_AddCreditCardOverlay_Heading", "Profile\\PaymentSettings.properties").isDisplayed());
sa.assertTrue(l1.getWebElement("PaymentSettings_NameOnCard_Label", "Profile\\PaymentSettings.properties").isDisplayed());
sa.assertTrue(l1.getWebElement("PaymentSettings_Type_Label", "Profile\\PaymentSettings.properties").isDisplayed());
sa.assertTrue(l1.getWebElement("PaymentSettings_Number_Label", "Profile\\PaymentSettings.properties").isDisplayed());
sa.assertTrue(l1.getWebElement("PaymentSettings_Expires_Label", "Profile\\PaymentSettings.properties").isDisplayed());
//sa.assertTrue(l1.getWebElement("PaymentSettings_Payment_List", "Profile\\PaymentSettings.properties").isDisplayed());
//sa.assertTrue(l1.getWebElement("PaymentSettings_DeleteCard_Link", "Profile\\PaymentSettings.properties").isDisplayed()); 
                         
}
		
@Test(groups="{Regression}",description="124784,124785,124781,124783,128535,128723")
public void TC01_CancelCard_and_AddCreditCard() throws Exception
{

log.info("Click on Payment method link");
Thread.sleep(5000);
l1.getWebElement("PaymentSettings_Link_MyaccountLandingPage","Profile\\PaymentSettings.properties").click();
log.info("Click on add credit card button");
Thread.sleep(3000);
l1.getWebElement("PaymentSettings_AddCreditCard_Button","Profile\\PaymentSettings.properties").click();
log.info("select Credit card button ");
l1.getWebElement("PaymentSetting_Type_CC","Profile\\PaymentSettings.properties").click();
Select dropdown=new Select(l1.getWebElement("PaymentSetting_ListOfCC","Profile\\PaymentSettings.properties"));
dropdown.selectByIndex(1);
log.info("Entering values");
p.CreateCreditCard("data\\ProfileData.xls","CreditCard",1);
log.info("Click on cancel button in overlay");
l1.getWebElement("PaymentSettings_CancelButton","Profile\\PaymentSettings.properties").click();
sa.assertTrue(gVar.assertNotVisible("PaymentSettings_Overlay","Profile\\PaymentSettings.properties"),"Credit Card Overlay Not Visible");
log.info("Click on add credit card button");
l1.getWebElement("PaymentSettings_AddCreditCard_Button","Profile\\PaymentSettings.properties").click();
log.info("Click on close button in overlay");
l1.getWebElement("PaymentSettings_Overlay_CloseButton","Profile\\PaymentSettings.properties").click();
sa.assertTrue(gVar.assertNotVisible("PaymentSettings_Overlay","Profile\\PaymentSettings.properties"),"Credit Card Overlay Not Visible");
log.info("Click on add credit card button");
l1.getWebElement("PaymentSettings_AddCreditCard_Button","Profile\\PaymentSettings.properties").click();
sa.assertTrue(gVar.assertVisible("PaymentSettings_Overlay","Profile\\PaymentSettings.properties"),"Credit Card Overlay");
p.CreateCreditCard("data\\ProfileData.xls","CreditCard",1);
l1.getWebElement("PaymentSettings_ApplyButton","Profile\\PaymentSettings.properties").click();

sa.assertTrue(l1.getWebElement("PaymentSettings_Payment_List", "Profile\\PaymentSettings.properties").isDisplayed());
sa.assertTrue(gVar.assertVisible("PaymentSettings_CardHolderName","Profile\\PaymentSettings.properties"),"Card holder name");
sa.assertTrue(gVar.assertVisible("PaymentSettings_ccType","Profile\\PaymentSettings.properties"),"cc type");
sa.assertTrue(gVar.assertVisible("PaymentSettings_ccNumber","Profile\\PaymentSettings.properties"),"cc Number");
sa.assertTrue(gVar.assertVisible("PaymentSettings_ccExpire","Profile\\PaymentSettings.properties"),"cc Expire");
sa.assertTrue(gVar.assertVisible("PaymentSettings_DeleteCard","Profile\\PaymentSettings.properties"),"Delete card");
sa.assertEquals(gVar.assertEqual("PaymentSettings_CardHolderName","Profile\\PaymentSettings.properties"),GetData.getDataFromExcel("\\data\\ProfileData.xls","CreditCard",4,0),"Comparing card holder name");
sa.assertEquals(gVar.assertEqual("PaymentSettings_ccType","Profile\\PaymentSettings.properties"),GetData.getDataFromExcel("\\data\\ProfileData.xls","CreditCard",4,1),"Comparing card name");
sa.assertEquals(gVar.assertEqual("PaymentSettings_ccExpire","Profile\\PaymentSettings.properties"),GetData.getDataFromExcel("\\data\\ProfileData.xls","CreditCard",4,2),"Comparing Expire name");

}


@Test(groups="{Regression}",description="124790,128534")
public void TC03_DeleteCard() throws Exception
{
	
log.info("Deleting the added card");
p.DeleteCreditCard();
	
}
		
@Test(groups="{Regression}",description="124779")
public void TC04_PaymentSettings_LeftNav_navigation() throws Exception
{
	
	p.loginToAppliction(l1);

	//left nav links	
	log.info("Take Count of left nav links");
	int LeftNavLinksCount=l1.getWebElements("LeftNav_Links","Profile\\login.properties").size();
	
	for(int i=0;i<LeftNavLinksCount;i++)
	{	
        //Navigate to wish list search result page
		log.info("click on my account link");
		l1.getWebElement("Header_MyAccount_Link","Profile\\MyAccountHome.properties").click();
		log.info("Click on personal data link");
		l1.getWebElement("MyAccount_PaymentSettings","Profile\\MyAccountHome.properties").click();
		
		//Link Display verification
		sa.assertTrue(l1.getWebElements("LeftNav_Links","Profile\\login.properties").get(i).isDisplayed(),"LeftNav links display");
		String leftNavLinks=l1.getWebElements("LeftNav_Links","Profile\\login.properties").get(i).getText();
		sa.assertEquals(leftNavLinks,GetData.getDataFromExcel("\\data\\ProfileData.xls","WishList",i+1,4));
		
		//click on left nav links 
		log.info("Click on left nav link: "+i);
		l1.getWebElements("LeftNav_Links","Profile\\login.properties").get(i).click();
		
		String Expected_LeftnavHeading = GetData.getDataFromExcel("\\data\\ProfileData.xls", "LeftNav", i+1, 3);
		//String Expected_LeftnavBreadcrumb = GetData.getDataFromExcel("\\data\\ProfileData.xls", "LeftNav", i+1, 4);
		//verify the navigation
		sa.assertEquals(l1.getWebElement("PageHeadings", "Profile\\Wishlist.properties").getText(), Expected_LeftnavHeading);
		//sa.assertEquals(l1.getWebElements("PageBreadcrumb", "Profile\\Wishlist.properties").get(1).getText(), Expected_LeftnavBreadcrumb);
	}
	
       //Navigate to wish list search result page
		log.info("click on my account link");
		l1.getWebElement("Header_MyAccount_Link","Profile\\MyAccountHome.properties").click();
		log.info("Click on personal data link");
		l1.getWebElement("MyAccount_PaymentSettings","Profile\\MyAccountHome.properties").click();
	
		log.info("Take Count of left nav Headings");				
		int LeftNavHeadings=l1.getWebElements("LeftNav_Headings","Profile\\login.properties").size();
		for (int i=0;i<=LeftNavHeadings;i++)
		{
			log.info("Left nav headings");
			sa.assertEquals(l1.getWebElements("LeftNav_Headings","Profile\\login.properties").get(i).getText(),GetData.getDataFromExcel("\\data\\ProfileData.xls","WishList",i+1,5));
		}
		
	log.info("Contact-US link");
	l1.getWebElement("Wishlist_ContactUS_link", "Profile\\Wishlist.properties").click();
	sa.assertEquals(l1.getWebElement("PageHeadings", "Profile\\Wishlist.properties").getText(), GetData.getDataFromExcel("\\data\\ProfileData.xls", "WishList", 4, 6));
	sa.assertEquals(l1.getWebElements("PageBreadcrumb", "Profile\\Wishlist.properties").get(1).getText(), GetData.getDataFromExcel("\\data\\ProfileData.xls", "WishList", 4, 7));
	//It will return all the assertions
	sa.assertAll();
		}

	
}
	

