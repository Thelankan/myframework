package com.cbk.profile;


import static org.testng.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.cbk.projectspec.GlobalFunctions;
import com.cbk.projectspec.ProfileFunctions;
import com.cbk.utilgeneric.BaseTest;
import com.cbk.utilgeneric.Data;
import com.cbk.utilgeneric.GetData;
import com.cbk.utilgeneric.GetDriver;
import com.cbk.utilgeneric.TestData;

public class Address extends BaseTest{
	
@Test(groups="{Regression}",description="288240_124764_124763_124761_124767_124766")
public void TC01_AddressOverlayUI() throws Exception
{
	
	log.info("Login");
	p.loginToAppliction(l1);
	log.info("app logged in");
	Thread.sleep(3000);
	log.info("Click on Address link");
	l1.getWebElement("Address_Link_AccountLandingPage","Profile\\Addresses.properties").click();
	//Address title
	sa.assertTrue(gVar.assertVisible("Address_heading", "Profile\\Addresses.properties"),"Address Heading");
	//Create Address buttons
	sa.assertTrue(gVar.assertVisible("Address_CreateNew_Button", "Profile\\Addresses.properties"),"Create Address button");
	//Create New Address
	log.info("Click on Create New Address button ");
	l1.getWebElement("Address_CreateNew_Button","Profile\\Addresses.properties").click();
	log.info("Verify the UI of address overlay");
	//To check elements are visible or not 
	 sa.assertTrue(gVar.assertVisible("Address_Overlay_Close", "Profile\\Addresses.properties"),"Address overlay close icon");
	 sa.assertTrue(gVar.assertVisible("Address_Overlay_Heading", "Profile\\Addresses.properties"),"Address_Overlay_Heading");
	 sa.assertTrue(gVar.assertVisible("Address_Overlay_RequiredText", "Profile\\Addresses.properties"),"Address_Overlay_RequiredText");
	 sa.assertTrue(gVar.assertVisible("Address_FirstName_Label", "Profile\\Addresses.properties"),"Address_FirstName_Label");
	 sa.assertTrue(gVar.assertVisible("Address_FirstName", "Profile\\Addresses.properties"),"Address_FirstName");
	 sa.assertTrue(gVar.assertVisible("Address_LastName_Label", "Profile\\Addresses.properties"),"Address_LastName_Label");
	 sa.assertTrue(gVar.assertVisible("Address_LastName", "Profile\\Addresses.properties"),"Address_LastName");
	 sa.assertTrue(gVar.assertVisible("Address_Address1_Label", "Profile\\Addresses.properties"),"Address_Address1_Label");
	 sa.assertTrue(gVar.assertVisible("Address_Address1", "Profile\\Addresses.properties"),"Address_Address1 textbox");
	 sa.assertTrue(gVar.assertVisible("Address_Overlay_Address1StaticText", "Profile\\Addresses.properties"));
	 sa.assertTrue(gVar.assertVisible("Address_Address2_Label", "Profile\\Addresses.properties"),"Address_Address2_Label");
	 sa.assertTrue(gVar.assertVisible("Address_Address2", "Profile\\Addresses.properties"),"Address_Address2 textbox");
	 sa.assertTrue(gVar.assertVisible("Address_City_Label", "Profile\\Addresses.properties"),"Address_City_Label");
	 sa.assertTrue(gVar.assertVisible("Address_City", "Profile\\Addresses.properties"),"Address_City textbox");
	 sa.assertTrue(gVar.assertVisible("Address_State_Label", "Profile\\Addresses.properties"),"Address_State_Label");
	 sa.assertTrue(gVar.assertVisible("Address_State", "Profile\\Addresses.properties"),"Address_State dropdown");
	 sa.assertTrue(gVar.assertVisible("Address_Postal_Label", "Profile\\Addresses.properties"),"Address_Postal_Label");
	 sa.assertTrue(gVar.assertVisible("Address_Postal", "Profile\\Addresses.properties"),"Address_Postal textbox");
	 sa.assertTrue(gVar.assertVisible("Address_Country_Label", "Profile\\Addresses.properties"),"Address_Country_Label");
	 sa.assertTrue(gVar.assertVisible("Address_Country", "Profile\\Addresses.properties"),"Address_Country dropdown");
	 sa.assertTrue(gVar.assertVisible("Address_Phone_Label", "Profile\\Addresses.properties"),"Address_Phone_Label");
	 sa.assertTrue(gVar.assertVisible("Address_Overlay_WhyThisRequired", "Profile\\Addresses.properties"),"Address_Overlay_WhyThisRequired");
	 sa.assertTrue(gVar.assertVisible("Address_phone", "Profile\\Addresses.properties"),"Address_phone Textbox");
	 sa.assertTrue(gVar.assertVisible("Address_ApplyButton", "Profile\\Addresses.properties"),"Address_ApplyButton");
	 sa.assertTrue(gVar.assertVisible("Address_CancelButton", "Profile\\Addresses.properties"),"Address_CancelButton");
	//Close
	 Thread.sleep(4000);
	 log.info("Close Address_Overlay");
	l1.getWebElement("Address_Overlay_Close", "Profile\\Addresses.properties").click();
	try {
		if(l1.getWebElement("Address_Overlay", "Profile\\Addresses.properties").isDisplayed()){
			System.out.println("Address Overlay is not closed");
		}
	} catch (Exception e) {
		System.out.println("Address Overlay closed");
	}
	//Create New Address
	log.info("Click Create Address");
	l1.getWebElement("Address_CreateNew_Button","Profile\\Addresses.properties").click();
	log.info("Clear fiels");
	p.ClearAddressFiles();
	//Enter address
	log.info("Enter Address 2");
	p.EnterAddressValues("\\data\\ProfileData.xls","Address",2);
	//cancel
	 log.info("Close Address_Overlay");
	l1.getWebElement("Address_Overlay_Close", "Profile\\Addresses.properties").click();
		try {
			if(l1.getWebElement("Address_Overlay", "Profile\\Addresses.properties").isDisplayed()){
				System.out.println("Address Overlay is not closed");
			}
		} catch (Exception e) {
			System.out.println("Address Overlay closed");
		}

		Thread.sleep(3000);
    //Address list
	sa.assertFalse(gVar.assertVisible("Address_List", "Profile\\Addresses.properties"));
	log.info("verified address list");
	//It will return all the assertions
	sa.assertAll();
	
}

@Test(groups="{Regression}",description="124770_288239_124775_148670_124775_124777_124757_148672")
public void TC02_EnterAddress_EditAddress_link() throws Exception
{	
	
	log.info("Click on Address link");
	l1.getWebElement("Address_Link_AccountLandingPage","Profile\\Addresses.properties").click();
	//Create New Address
	log.info("Click Create Address");
	l1.getWebElement("Address_CreateNew_Button","Profile\\Addresses.properties").click();
	Thread.sleep(2000);
	log.info("Enter address1");
	p.EnterAddressValues("\\data\\ProfileData.xls","Address",1);
	log.info("Click on Apply button");
	l1.getWebElement("Address_ApplyButton","Profile\\Addresses.properties").click();
	Thread.sleep(2000);
	//Default Address heading
	sa.assertTrue(gVar.assertVisible("Address_DefaultAddress_Heading", "Profile\\Addresses.properties"),"Address_DefaultAddress_Heading");
	//Address Title
	sa.assertEquals(gVar.assertEqual("Addsress_Default_Title", "Profile\\Addresses.properties"), GetData.getDataFromExcel("\\data\\ProfileData.xls","Address",1,10));
	//FN LN
	sa.assertEquals(gVar.assertEqual("Addsress_Fname_Lname", "Profile\\Addresses.properties"), GetData.getDataFromExcel("\\data\\ProfileData.xls","Address",1,11));
	//Mini-address
	sa.assertEquals(gVar.assertEqual("Address_DefaultAddress", "Profile\\Addresses.properties").toString(), GetData.getDataFromExcel("\\data\\ProfileData.xls","Address",1,12));
	//Delete link
	sa.assertTrue(gVar.assertVisible("Address_Delete_Link", "Profile\\Addresses.properties"),"Delete Address link");
	//Edit link
	sa.assertTrue(gVar.assertVisible("Address_Edit_Link", "Profile\\Addresses.properties"),"Edit Address link");
	log.info("Click on Edit link");
	l1.getWebElement("Address_Edit_Link", "Profile\\Addresses.properties").click();
	Thread.sleep(1000);
	//Edit address overlay
	//To check elements are visible or not 
	 sa.assertTrue(gVar.assertVisible("Address_Overlay_Close", "Profile\\Addresses.properties"),"Address overlay close icon");
	 sa.assertTrue(gVar.assertVisible("Address_Edit_Overlay_Heading", "Profile\\Addresses.properties"),"Address_Overlay_Heading");
	 sa.assertTrue(gVar.assertVisible("Address_Overlay_RequiredText", "Profile\\Addresses.properties"),"Address_Overlay_RequiredText");
	 sa.assertTrue(gVar.assertVisible("AddressName_Label", "Profile\\Addresses.properties"),"AddressName_Label");
	 sa.assertTrue(gVar.assertVisible("Address_AddressName", "Profile\\Addresses.properties"),"Address_AddressName textbox");
	 sa.assertTrue(gVar.assertVisible("Address_FirstName_Label", "Profile\\Addresses.properties"),"Address_FirstName_Label");
	 sa.assertTrue(gVar.assertVisible("Address_FirstName", "Profile\\Addresses.properties"),"Address_FirstName");
	 sa.assertTrue(gVar.assertVisible("Address_LastName_Label", "Profile\\Addresses.properties"),"Address_LastName_Label");
	 sa.assertTrue(gVar.assertVisible("Address_LastName", "Profile\\Addresses.properties"),"Address_LastName");
	 sa.assertTrue(gVar.assertVisible("Address_Address1_Label", "Profile\\Addresses.properties"),"Address_Address1_Label");
	 sa.assertTrue(gVar.assertVisible("Address_Address1", "Profile\\Addresses.properties"),"Address_Address1 textbox");
	 sa.assertTrue(gVar.assertVisible("Address_Overlay_Address1StaticText", "Profile\\Addresses.properties"));
	 sa.assertTrue(gVar.assertVisible("Address_Address2_Label", "Profile\\Addresses.properties"),"Address_Address2_Label");
	 sa.assertTrue(gVar.assertVisible("Address_Address2", "Profile\\Addresses.properties"),"Address_Address2 textbox");
	 sa.assertTrue(gVar.assertVisible("Address_City_Label", "Profile\\Addresses.properties"),"Address_City_Label");
	 sa.assertTrue(gVar.assertVisible("Address_City", "Profile\\Addresses.properties"),"Address_City textbox");
	 sa.assertTrue(gVar.assertVisible("Address_State_Label", "Profile\\Addresses.properties"),"Address_State_Label");
	 sa.assertTrue(gVar.assertVisible("Address_State", "Profile\\Addresses.properties"),"Address_State dropdown");
	 sa.assertTrue(gVar.assertVisible("Address_Postal_Label", "Profile\\Addresses.properties"),"Address_Postal_Label");
	 sa.assertTrue(gVar.assertVisible("Address_Postal", "Profile\\Addresses.properties"),"Address_Postal textbox");
	 sa.assertTrue(gVar.assertVisible("Address_Country_Label", "Profile\\Addresses.properties"),"Address_Country_Label");
	 sa.assertTrue(gVar.assertVisible("Address_Country", "Profile\\Addresses.properties"),"Address_Country dropdown");
	 sa.assertTrue(gVar.assertVisible("Address_Phone_Label", "Profile\\Addresses.properties"),"Address_Phone_Label");
	 sa.assertTrue(gVar.assertVisible("Address_Overlay_WhyThisRequired", "Profile\\Addresses.properties"),"Address_Overlay_WhyThisRequired");
	 sa.assertTrue(gVar.assertVisible("Address_Overlay_PhoneEx", "Profile\\Addresses.properties"),"Address_Overlay_PhoneEx");
	 sa.assertTrue(gVar.assertVisible("Address_phone", "Profile\\Addresses.properties"),"Address_phone Textbox");
	 sa.assertTrue(gVar.assertVisible("Address_ApplyButton", "Profile\\Addresses.properties"),"Address_ApplyButton");
	 sa.assertTrue(gVar.assertVisible("Address_CancelButton", "Profile\\Addresses.properties"),"Address_CancelButton");
	 //Address name
	sa.assertEquals(gVar.assertEqual("Address_AddressName", "Profile\\Addresses.properties", "value"), GetData.getDataFromExcel("\\data\\ProfileData.xls","Address",1,0));
	//Firstname
	sa.assertEquals(gVar.assertEqual("Address_FirstName", "Profile\\Addresses.properties", "value"), GetData.getDataFromExcel("\\data\\ProfileData.xls","Address",1,1));
	//lastname
	sa.assertEquals(gVar.assertEqual("Address_LastName", "Profile\\Addresses.properties", "value"), GetData.getDataFromExcel("\\data\\ProfileData.xls","Address",1,2));
	//Address1
	sa.assertEquals(gVar.assertEqual("Address_Address1", "Profile\\Addresses.properties", "value"), GetData.getDataFromExcel("\\data\\ProfileData.xls","Address",1,3));
	//Address2
	sa.assertEquals(gVar.assertEqual("Address_Address2", "Profile\\Addresses.properties", "value"), GetData.getDataFromExcel("\\data\\ProfileData.xls","Address",1,4));
	//City
	sa.assertEquals(gVar.assertEqual("Address_City", "Profile\\Addresses.properties", "value"), GetData.getDataFromExcel("\\data\\ProfileData.xls","Address",1,5));
	//State
	sa.assertEquals((new Select(l1.getWebElement("Address_State", "Profile\\Addresses.properties"))).getFirstSelectedOption(), GetData.getDataFromExcel("\\data\\ProfileData.xls","Address",1,6));
	//Postal
	sa.assertEquals(gVar.assertEqual("Address_Postal", "Profile\\Addresses.properties", "value"), GetData.getDataFromExcel("\\data\\ProfileData.xls","Address",1,7));
	//Country
	sa.assertEquals((new Select(l1.getWebElement("Address_Country", "Profile\\Addresses.properties"))).getFirstSelectedOption(), GetData.getDataFromExcel("\\data\\ProfileData.xls","Address",1,8));
	//Phone
	sa.assertEquals(gVar.assertEqual("Address_phone", "Profile\\Addresses.properties", "value"), GetData.getDataFromExcel("\\data\\ProfileData.xls","Address",1,9));
	//Close
	log.info("Click on Address overlay close");
	l1.getWebElement("Address_Overlay_Close", "Profile\\Addresses.properties").click();		
	//sa.assertFalse(gVar.assertVisible("Address_Overlay", "Profile\\Addresses.properties"),"");
	sa.assertTrue(gVar.assertNotVisible("Address_Overlay", "Profile\\Addresses.properties"),"Address overlay should close");
	//Edit address
	log.info("Click Address Edit link");
	l1.getWebElement("Address_Edit_Link", "Profile\\Addresses.properties").click();
  	log.info("Clear Fields");
    p.ClearAddressFiles();
	//Address 2
	log.info("Enter Address 2");
	p.EnterAddressValues("\\data\\ProfileData.xls","Address",2);
	//cancel
	log.info("click on Cancel");
	l1.getWebElement("Address_CancelButton", "Profile\\Addresses.properties").click();
	Thread.sleep(1000);
	//Address Title
	sa.assertNotEquals(gVar.assertEqual("Addsress_Default_Title", "Profile\\Addresses.properties"), GetData.getDataFromExcel("\\data\\ProfileData.xls","Address",2,10));
	//FN LN
	sa.assertNotEquals(gVar.assertEqual("Addsress_Fname_Lname", "Profile\\Addresses.properties"), GetData.getDataFromExcel("\\data\\ProfileData.xls","Address",2,11));
	//Mini-address
	sa.assertNotEquals(gVar.assertEqual("Address_DefaultAddress", "Profile\\Addresses.properties"), GetData.getDataFromExcel("\\data\\ProfileData.xls","Address",2,12));
	//It will return all the assertions
	sa.assertAll();
}

@Test(groups="{Regression}",description="124778")
public void TC03_EditAddress() throws Exception
{

	log.info("Click on Address link");
	l1.getWebElement("Address_Link_AccountLandingPage","Profile\\Addresses.properties").click();

	log.info("Click on Address link");
	l1.getWebElement("Address_Link_AccountLandingPage","Profile\\Addresses.properties").click();
	//Edit address
	log.info("Click on Edit Address");
	l1.getWebElement("Address_Edit_Link", "Profile\\Addresses.properties").click();
	Thread.sleep(2000);
  	log.info("Clear Fields");
    p.ClearAddressFiles();
	//Address 2
	log.info("Enter Address 2");
	p.EnterAddressValues("\\data\\ProfileData.xls","Address",2);
	//Apply
	log.info("Click on Apply button");
	l1.getWebElement("Address_ApplyButton","Profile\\Addresses.properties").click();
	Thread.sleep(5000);
	//Default Address heading
	sa.assertTrue(gVar.assertVisible("Address_DefaultAddress_Heading", "Profile\\Addresses.properties"),"Address_DefaultAddress_Heading");
	//Address Title
	sa.assertEquals(gVar.assertEqual("Addsress_Default_Title", "Profile\\Addresses.properties"), GetData.getDataFromExcel("\\data\\ProfileData.xls","Address",2,10));
	//FN LN
	sa.assertEquals(gVar.assertEqual("Addsress_Fname_Lname", "Profile\\Addresses.properties"), GetData.getDataFromExcel("\\data\\ProfileData.xls","Address",2,11));
	//Mini-address
	sa.assertEquals(gVar.assertEqual("Address_DefaultAddress", "Profile\\Addresses.properties"), GetData.getDataFromExcel("\\data\\ProfileData.xls","Address",2,12));
	//Delete link
	Thread.sleep(4000);
	sa.assertTrue(gVar.assertVisible("Address_Delete_Link", "Profile\\Addresses.properties"),"Address_Delete_Link");
	Thread.sleep(4000);
	//Edit link
    sa.assertTrue(gVar.assertVisible("Address_Edit_Link", "Profile\\Addresses.properties"),"Address_Edit_Link");
	//It will return all the assertions
	sa.assertAll();
}


@Test(groups="{Regression}",description="288241/124771")
public void TC12_Deleting_Saved_Address_Address() throws Exception
{
	log.info("Click on Address link");
	l1.getWebElement("Address_Link_AccountLandingPage","Profile\\Addresses.properties").click();
	log.info("Delete Saved address");
	p.DeleteAddress();
	sa.assertTrue(gVar.assertNotVisible("Address_List", "Profile\\Addresses.properties"),"No Addresses should display");
	//It will return all the assertions
	sa.assertAll();
}

}