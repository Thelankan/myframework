package com.cbk.cart;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.cbk.pom.CartPOM;
import com.cbk.pom.MiniCartPOM;
import com.cbk.utilgeneric.BaseTest;
public class Cart extends BaseTest{
	
	
@Test(groups="{Regression}",description="208185")
public void TC01_deleteTheLineItem() throws Exception{
	
	cart.navigateToCart();
	Thread.sleep(3000);
	l1.getWebElements(CartPOM.Cart_LineItem_Remove_Link).get(0).click();
	Thread.sleep(3000);
	log.info("product got deleted");
	Thread.sleep(5000);
	WebElement cartquantity=l1.getWebElement(MiniCartPOM.MiniCartEmptyLink);
	log.info("product get text");
	log.info("the value of mini cart is :-  "+cartquantity.getText());
	
}

@Test(groups="{Regression}",description="208191")
public void TC02_AddToLineTteamToWishLisht() throws Exception
{
	
	//login to the application
	p.loginToAppliction(l1);
	cart.navigateToCart();
	Thread.sleep(5000);
	log.info("clicks on wishlist link");
	l1.getWebElement(CartPOM.Wishlist_link).click();
	Thread.sleep(5000);
	l1.getWebElement(CartPOM.Wishlistoverly_ContinueButton).click();
	Thread.sleep(3000);
	l1.getWebElement(CartPOM.WishlistOverlysuccess_OkButton).click();
	Thread.sleep(3000);
	log.info("navigate to empty cart page");
	String EmptyCart_text=l1.getWebElement(CartPOM.YoutShoppingCartIsEmpty_text).getText();
	String Expected_text="Your Shopping Cart is Empty";
	Assert.assertEquals(EmptyCart_text, Expected_text);
		
}

	
@Test(groups="{Regression}",description="209659")
public void TC03_VerifyTheEditlink() throws Exception
{
	
	cart.navigateToCart();
	Thread.sleep(3000);
	l1.getWebElement(CartPOM.edit_link).click();
	Thread.sleep(3000);
	l1.getWebElement(CartPOM.ProductName_text).getText();
	l1.getWebElement(CartPOM.Productitem_no).getText();
	sa.assertTrue(gVar.assertVisible(CartPOM.UpdateBag_button),"update bag button");
	
}
	
	
@Test(groups="{Regression}",description="228063")
public void TC04_VerifyShopRunnerExpressCheckout() throws Exception
{
	
	cart.navigateToCart();
	Thread.sleep(3000);
	l1.getWebElement(CartPOM.edit_link).click();
	Thread.sleep(3000);
	l1.getWebElement(CartPOM.ShopRunner_button).click();
	Thread.sleep(3000);
	sa.assertTrue(gVar.assertVisible(CartPOM.SignInOverlay), "Sing in overlay ");
	
}
	
}
	
