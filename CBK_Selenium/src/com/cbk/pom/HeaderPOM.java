package com.cbk.pom;

import org.openqa.selenium.By;

public class HeaderPOM {
	
	public static By headerPromobar=By.xpath("//div[@class='banner-container']");
	public static By headerLoginLink=By.xpath("//span[contains(text(),'Login')]");
	public static By headerStoreslink=By.xpath("//span[@class='storename']");
	public static By headerCreditcardlink=By.xpath("//span[contains(text(),'credit card')]");
	public static By headerSociallink=By.xpath("//span[contains(text(),'social')]");
	public static By headerorderstatuslink=By.xpath("//li[@class='orderinfo-status']/a");
	public static By headerhelplink=By.xpath("//ul[@class='menu-utility-user menu-content-border']//span[contains(text(),'help')]");
	public static By headerSearchtextbox=By.xpath("//input[@id='q']");
	public static By headerHomepagelogo=By.xpath("//img[@alt='Christopher & Banks']");
	public static By headerMinicartEmpty=By.xpath("//a[@class='mini-cart-link mini-cart-empty']");
	public static By headerMinicartQuantity=By.xpath("//a[@class='minicart-quantity']");
	public static By headerMinicartemptyHeading=By.xpath("//div[@class='cart-empty']//h1");
	public static By headerDropdownSymboolPlus=By.xpath("//span[@class='icon']//i[@class='fa fa-plus']");
	public static By headerDropdownSymboolMinus=By.xpath("(//span[@class='icon']//i[@class='fa fa-plus'])[1]");
	public static By headerPromodetailsoverlay=By.xpath("//div[@class='promo-details-text']");
	public static By headerLoginform=By.xpath("//form[@id='header-login-form']");
	public static By headerStorelocatorHeading=By.xpath("//h1[@class='title-heading']//span[contains(text(),'Find Your Local')]");
	public static By headeruppernavlinks=By.xpath("//ul[@class='menu-utility-user menu-content-border']/li/a");
	public static By headercreditheading=By.xpath("//h1[@class='heding-earnpoint']");
	public static By headerSocialLinkwrapper=By.xpath("//div[@class='social-main-wrapper']");
	public static By headerViewOrderHistory=By.xpath("//h2[contains(@class,'view-Order')]");
	public static By headerCustomerservice=By.xpath("//h1[contains(text(),'Customer Service')]");
	public static By headerCategoryLinks=By.xpath("//a[@class='has-sub-menu']");
	public static By headerBreadcrumbelement=By.xpath("//a[@class='breadcrumb-element']");
	public static By headerMyaccountHeading=By.xpath("//span[contains(text(),'My Account')]");

	//public static By headerPromodetailsoverlay=By.xpath("//div[@class='promo-details-text']");
	//public static By headerCreditcardlink=By.xpath("//li[@class='orderinfo-status']/a");
	
}
