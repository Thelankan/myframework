package com.cbk.pom;

import org.openqa.selenium.By;

public class CheckoutIlpPOM {
	
	
	public static By Ilp_CheckoutHeading=By.xpath("//h2[@class='checkout-title']");
	public static By Ilp_CheckoutasGuestButton=By.xpath("//button[@name='dwfrm_login_unregistered']");
	public static By Ilp_checkoutloginButton=By.xpath("//div[contains(@class,'checkout-login button')]");
	public static By Ilp_usernametextbox=By.xpath("//div[@class='checkoutlogin']//input[contains(@id,'dwfrm_login_username')]");
	public static By Ilp_passwordtbox=By.xpath("//div[@class='checkoutlogin']//input[contains(@id,'dwfrm_login_password')]");
	public static By Ilp_loginbutton=By.xpath("//button[@class='spc-login-btn']");
	public static By Ilp_entiresection=By.xpath("//div[@class='checkoutlogin']");
	public static By Ilp_ReturningCustomersSection=By.xpath("//div[@class='login-box login-account']");
	public static By Ilp_CreateAccountNowButton=By.xpath("//button[@name='dwfrm_login_register']");
	public static By Ilp_SocialMediaLinks=By.xpath("//form[@id='dwfrm_oauthlogin']");
	public static By Ilp_EmailPassword_ErrorMessaages=By.xpath("//span[@class='error']");
	public static By Ilp_EditLink_OrderSummary=By.linkText("Edit");
	public static By Ilp_ProductNameLink=By.xpath("//div[@id='secondary']//div[@class='mini-cart-name']");
	public static By Ilp_ProductImage=By.xpath("//div[@id='secondary']//div[@class='mini-cart-image']/img[@class='desktop-only']");
	public static By Ilp_frogetpasswordlink=By.xpath("//form[@id='dwfrm_login']//a[@id='password-reset']");
	public static By Ilp_frogetpasswordDialoguetext=By.xpath("//div[@id='dialog-container']/h1");
	public static By Ilp_ForgotPassword_CloseButton=By.xpath("//button[@title='Close']");
	public static By Ilp_ForgotPassword_Emailtextbox=By.id("dwfrm_requestpassword_email");
	public static By Ilp_ForgotPassword_sendbutton=By.xpath("//div[@id='dialog-container']//button[@type='submit']");
	public static By Ilp_receivepasswordheading=By.xpath("//p[contains(text(),'receive an email to reset your password')]");
	
	
	
}
