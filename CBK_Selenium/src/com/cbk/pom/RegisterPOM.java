package com.cbk.pom;

import org.openqa.selenium.By;

public class RegisterPOM {
	
	
	public By Login_CreateaccountFNtextbox=By.id("dwfrm_profile_customer_firstname");
	
	public static By Register_FirstName=By.id("dwfrm_profile_customer_firstname");
	public static By Register_LastName=By.id("dwfrm_profile_customer_lastname");
	public static By Register_Street=By.xpath("//input[contains(@id,'dwfrm_profile_address_registeraddress1')]");
	public static By Register_City=By.xpath("//input[contains(@id,'dwfrm_profile_address_registercity')]");
	public static By Register_State=By.xpath("//select[contains(@id,'dwfrm_profile_address_states_state')]");
	public static By Register_Zipcode=By.xpath("//input[contains(@id,'dwfrm_profile_address_postal')]");
	public static By Register_EmailID=By.id("dwfrm_profile_customer_email");
	public static By Register_ConfirmEmail=By.id("dwfrm_profile_customer_emailconfirm");
	public static By Register_PhoneNumber=By.id("dwfrm_profile_address_registerphone");
	public static By Register_PhoneType=By.id("dwfrm_profile_address_phonetype");
	public static By Register_Month=By.id("dwfrm_profile_customer_month");
	public static By Register_Day=By.id("dwfrm_profile_customer_day");
	public static By Register_Password_Confirmpassword=By.xpath("//input[contains(@id,'dwfrm_profile_login_password')]");
	public static By Register_ApplyButton=By.xpath("//li[@class='user-info']/a");
	public static By Register_Heading=By.xpath("//legend[contains(text(),'Select or Enter Billing Address')]");
	public static By Register_RequiredHeading=By.xpath("//legend[contains(text(),'Select or Enter Billing Address')]");
	public static By Register_Myaccount_Elements=By.xpath("//legend[contains(text(),'Select or Enter Billing Address')]");
	public static By Register_Addme_Checkbox=By.xpath("//legend[contains(text(),'Select or Enter Billing Address')]");
	public static By Register_privacyPolicy_Link=By.xpath("//legend[contains(text(),'Select or Enter Billing Address')]");
	public static By Register_FN_Label=By.xpath("//label[@for='dwfrm_profile_customer_firstname']");
	public static By Register_LN_Label=By.xpath("//label[@for='dwfrm_profile_customer_lastname']");
	public static By Register_Email_Label=By.xpath("//label[@for='dwfrm_profile_customer_email']");
	public static By Register_ConfirmEmail_Label=By.xpath("//label[@for='dwfrm_profile_customer_emailconfirm']");
	public static By Register_Password_Label=By.xpath("//span[contains(text(),'Password')]");
	public static By Register_ConfirmPassword_Label=By.xpath("//span[contains(text(),'Confirm Password')]");
	public static By Register_CreateAccount_LeftNav=By.xpath("//a[@title='Create an Account']");
	public static By Register_CreateAccount_heading=By.xpath("//div[@class='nonfr-header']//h1[contains(text(),'Hello')]");

/*	
	Register_LoginConfirmation=xpath;//span[@class='account-logout']
	Register_CreateAccount_LeftNav=xpath;//a[@title='Create an Account']
	Register_Breadcrumb_Element=xpath;//a[@class='breadcrumb-element']
	Register_PrivacyPolicy=xpath;//a[@class='privacy-policy']
	Register_PrivacyPolicy_Overlay=xpath;//div[@aria-labelledby='ui-id-1']
	Register_PrivacyPolicy_heading=xpath;//h1[contains(text(),'Privacy Policy - body')]
	Register_PrivacyPolicy_CloseButton=xpath;//button[@title='Close']
	Register_FirstName_Errormessage=id;dwfrm_profile_customer_firstname-error
	Register_LastName_Errormessage=id;dwfrm_profile_customer_lastname-error
	Register_Email_Errormessage=id;dwfrm_profile_customer_email-error
	Register_ConfirmEmail_Errormessage=id;dwfrm_profile_customer_emailconfirm-error
	Register_Password_Errormessage=xpath;//span[contains(@id,'dwfrm_profile_login_password')]


	LeftNav_NeedHelp_Heading=xpath;//div[@class='content-asset']/h2[contains(text(),'Need Help?')]
	LeftNav_Needhelp_Text=xpath;//p[(contains(text(),'If you have any question'))]
	LeftNav_CustomerService=xpath;//div[@class='content-asset']/p[contains(text(),'Customer Service')]
	MyAccountIcon=xpath;//a[@class='user-account']
	Register_Icon=xpath;//a[contains(text(),'Register')]
	Login_Icon=xpath;//a[contains(text(),'Login')]
	LeftNav_Headings=xpath;//h1[@class='content-header']
	LeftNav_ContactUs_Link=xpath;//div[@class='account-nav-asset']//a[contains(text(),'Contact Us')]
	LeftNav_ContactUs_heading=xpath;//h1[contains(text(),'Contact Us')]
*/

}
