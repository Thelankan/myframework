package com.cbk.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BillingPOM {
	
	WebDriver driver;
	
	public BillingPOM(WebDriver driver) {
		this.driver=driver;
	}
	
	public By billingPage_Heading=By.xpath("//legend[contains(text(),'Billing Address')]");
	public By billingPage_FN_TextBox=By.id("dwfrm_billing_billingAddress_addressFields_firstName");
	public By billingPage_LN_TextBox=By.id("dwfrm_billing_billingAddress_addressFields_lastName");
	public By billingPage_Address1_TextBox=By.id("dwfrm_billing_billingAddress_addressFields_address1");
	public By billingPage_Address2_TextBox=By.id("dwfrm_billing_billingAddress_addressFields_address2");
	public By billingPage_City_TextBox=By.id("dwfrm_billing_billingAddress_addressFields_city");
	public By billingPage_ZipCode_TextBox=By.id("dwfrm_billing_billingAddress_addressFields_postal");
	public By billingPage_Country_Dropdown=By.xpath("//div[@class='custom_select']//select[@id='dwfrm_billing_billingAddress_addressFields_country']");
	public By billingPage_State_Dropdown=By.xpath("//div[@class='custom_select']//select[@id='dwfrm_billing_billingAddress_addressFields_states_state']");
	public By billingPage_Phone_TextBox=By.id("dwfrm_billing_billingAddress_addressFields_phone");
	public By billingPage_Email_Textbox=By.id("dwfrm_singleshipping_shippingAddress_addressFields_friendshipRewards_email");
	public By billingPage_addToEmail_Checkbox=By.id("dwfrm_billing_billingAddress_addToEmailList");
	
	
	public By billingPage_FN_Label=By.xpath("//span[contains(text(),'First Name')]");
	public By billingPage_LN_Label=By.xpath("//span[contains(text(),'Last Name')]");
	public By billingPage_Address1_Label=By.xpath("//span[contains(text(),'Address 1')]");
	public By billingPage_Address2_Label=By.xpath("//span[contains(text(),'Address 2')]");
	public By billingPage_City_Label=By.xpath("//span[contains(text(),'City')]");
	public By billingPage_ZipCode_Label=By.xpath("//span[contains(text(),'ZIP Code')]");
	public By billingPage_Country_Label=By.xpath("//span[contains(text(),'Country')]");
	public By billingPage_State_Label=By.xpath("//span[contains(text(),'State')]");
	public By billingPage_Phone_Label=By.xpath("//span[contains(text(),'Phone')]");
	
	
	public By billingPage_shipping_button = By.xpath("//h2[contains(text(),'Shipping')]");
	public By billingPage_continue_button= By.xpath("//button[@name='dwfrm_singleshipping_shippingAddress_save']");

	public By billingPage_header= By.xpath("//div[@class='mini-billing-address order-component-block']/h3");
	public By billingPage_heading= By.xpath("//legend[contains(text(),'Select or Enter Billing Address')]");
	public By billingPage_required_indicator= By.xpath("//legend[contains(text(),'Select or Enter Billing Address')]//span[@class='required-indicator']");

	//billing address
	public By billingPage_select_address_dropdown= By.xpath("//select[@id='dwfrm_billing_addressList']");
	public By billingPage_apofpo_tooltip= By.xpath("//a[contains(text(),'APO/FPO')]");
	public By billingPage_number_tooltip= By.xpath("//a[contains(text(),'Why is this required?')]");
	public By billingPage_phonenumber_text= By.xpath("//div[contains(text(),'Example: 333-333-3333')]");
	public By billingPage_payment_number_example= By.xpath("//div[@id='PaymentMethod_CREDIT_CARD']//div[contains(text(),'Example: 4111111111111111')]");
	public By billingPage_addtoemaillist_checkbox= By.id("dwfrm_billing_billingAddress_addToEmailList");
	public By billingPage_addtoaddress_checkbox= By.id("dwfrm_billing_billingAddress_addToAddressBook");

	//PAYMENT
	public By billingPage_payment_header= By.xpath("//h2[Contains(text(),'Payment')]");
	public By billingPage_payment_heading= By.xpath("//legend[contains(text(),'Select Payment Method')]");
	public By billingPage_payment_details= By.xpath("//div[starts-with(@class,'mini-payment-instrument')]/div[@class='details']");
	public By billingPage_paymenteditlink= By.xpath("//div[starts-with(@class,'mini-payment-instrument')]//a");

	public By billingPage_paymentCVNtooltiplink= By.xpath("//div[@id='PaymentMethod_CREDIT_CARD']//a[contains(text(),'What is this?')]");
	public By billingPage_privacypolicy= By.xpath("//a[@class='privacy-policy']");
	public By billingPage_privacypolicyDialog= By.xpath("//div[@id='dialog-container']");
	public By billingPage_privacypolicyDialog_container= By.id("ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-draggable");
	public By billingPage_privacypolicyDialog_header= By.xpath("//h1[@class='content-header privacy-policy']");
	public By billingPage_privacypolicyClose_button= By.xpath("//div[@class='ui-dialog ui-widget ui-widget-content ui-corner-all ui-front ui-draggable']//button[@title='Close']");
	public By billingPage_ordersummary_editlink= By.xpath("//h3[contains(text(),'Order Summary')]/a");
	//div[@class='checkout-mini-cart']//preceding-sibling::h3/a

	//Payment methods
	public By billingPage_paymentAmazonRadioButton=By.id("is-PayWithAmazon");
	public By billingPage_paymentAmexRadioButton=By.id("is-AMERICAN_EXPRESS");
	public By billingPage_paymentmastercardradiobutton=By.id("is-MASTERCARD");
	public By billingPage_paymentVisaRadioButton=By.id("is-VISA");
	public By billingPage_paymentcreditcardRadioButton=By.id("is-CREDIT_CARD");
	public By billingPage_paymentPaypalRadioButton=By.id("is-PayPal");
	public By billingPage_paymentBillMelaterRadioButton=By.id("is-BML");

	public By billingPage_paymentNameonCard= By.xpath("//div[@id='PaymentMethod_CREDIT_CARD']//span[contains(text(),'Name on Card')]");
	public By billingPage_paymentcardtype= By.xpath("//div[@id='PaymentMethod_CREDIT_CARD']//span[contains(text(),'Type')]");
	public By billingPage_paymentCardnumber= By.xpath("//div[@id='PaymentMethod_CREDIT_CARD']//span[contains(text(),'Number')]");
	public By billingPage_paymentExpirationLabel= By.xpath("//div[@id='PaymentMethod_CREDIT_CARD']//span[contains(text(),'Expiration Date:')]");
	public By billingPage_paymentSecurityCode= By.xpath("//div[@id='PaymentMethod_CREDIT_CARD']//span[contains(text(),'Security Code')]");
	public By billingPage_paymentSaveCardCheckbox= By.id("dwfrm_billing_paymentMethods_creditCard_saveCard");

	public By billingPage_paymentCardtypeDropdown= By.xpath("//select[@id='dwfrm_billing_paymentMethods_creditCard_type']");
	public By billingPage_paymentOwnerNameTextbox= By.xpath("//input[@id='new_dwfrm_billing_paymentMethods_creditCard_owner']");
	public By billingPage_paymentCreditCardTextBox= By.xpath("//div[@class='credit_fields']//input[contains(@id,'dwfrm_billing_paymentMethods_creditCard_number')]");
	public By billingPage_PaymentExpireMonthtextbox= By.xpath("//select[@id='new_dwfrm_billing_paymentMethods_creditCard_expiration_month']");
	public By billingPage_paymentExpireYeartextbox= By.xpath("//select[@id='new_dwfrm_billing_paymentMethods_creditCard_expiration_year']");
	public By billingPage_paymentCVVTextbox= By.xpath("//div[@class='cvv_empty']//input[contains(@id,'new_dwfrm_billing_paymentMethods_creditCard_cvn')]");
	public By billingPage_paymentOwnerNameCheckbox= By.id("dwfrm_billing_paymentMethods_creditCard_owner");
	
	public By billingPage_PrivacyPolicy = By.xpath("//a[@class='privacy-policy']");
	public By billingPagePrivacyPolicy_Overlay= By.xpath("//div[@aria-labelledby='ui-id-1']");
	public By billingPage_PrivacyPolicy_heading= By.xpath("//h1[contains(text(),'Privacy Policy - body')]");
	public By billingPage_PrivacyPolicy_CloseButton= By.xpath("//button[@title='Close']");
	public By shippingAccordion=By.xpath("//h2[contains(text(),'Shipping')]");
	public By OrderReviewAccordion=By.xpath("//h2[contains(text(),'Order Review')]");
	public By paymentAccordion=By.xpath("//h2[contains(text(),'Payment')]");
	public By BillingPage_OrderTotal=By.xpath("//div[@class='checkout-order-totals']//tr[@class='order-total']//td//following-sibling::td");
	public By BillingPage_PaymentSection_Tab_Opened=By.xpath("//div[@class='checkout-tab-head open']/h2[contains(text(),'Payment')]");
	
}
