package com.cbk.checkout;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.cbk.pom.BillingPOM;
import com.cbk.pom.CartPOM;
import com.cbk.pom.CheckoutIlpPOM;
import com.cbk.pom.MiniCartPOM;
import com.cbk.pom.OrderreviewPOM;
import com.cbk.pom.ShippingPOM;
import com.cbk.utilgeneric.BaseTest;
import com.cbk.utilgeneric.Data;
import com.cbk.utilgeneric.GetData;
import com.cbk.utilgeneric.TestData;

public class CheckoutIlp extends BaseTest {

	
@Test(groups="{Regression}",description="124962")
public void TC00_NavigatetoILP() throws Exception
{

log.info("Additem to cart");	
s.AddtoCart();
Thread.sleep(2000);
log.info("Click on go straight to checkot");
l1.getWebElement(MiniCartPOM.MiniCartGoStraightToCheckoutButton).click();
Thread.sleep(2000);
log.info("Checkoiut ILP heading");
sa.assertTrue(gVar.assertVisible(CheckoutIlpPOM.Ilp_CheckoutHeading));
sa.assertAll();

}


@Test(groups="{Regression}",description="124968")
public void TC01_ForgetPasswordLink() throws Exception
{

log.info("Click on login for faster checkout");
l1.getWebElement(CheckoutIlpPOM.Ilp_checkoutloginButton).click();
Thread.sleep(2000);
log.info("Click on forgotpassword link");
l1.getWebElement(CheckoutIlpPOM.Ilp_frogetpasswordlink).click();
log.info("Forgot password dialog");
sa.assertTrue(gVar.assertVisible(CheckoutIlpPOM.Ilp_frogetpasswordDialoguetext),"Forgot password dialog box verification");
log.info("Enter the valid email id");
l1.getWebElement(CheckoutIlpPOM.Ilp_ForgotPassword_Emailtextbox).sendKeys("vla@dummy.com");
log.info("Click on send button");
l1.getWebElement(CheckoutIlpPOM.Ilp_ForgotPassword_sendbutton).click();
log.info("verifying Heading");
sa.assertTrue(gVar.assertVisible(CheckoutIlpPOM.Ilp_receivepasswordheading),"password heading");
log.info("Click on close");
l1.getWebElement(CheckoutIlpPOM.Ilp_ForgotPassword_CloseButton).click();
log.info("Click on guet checkout button");
l1.getWebElement(CheckoutIlpPOM.Ilp_CheckoutasGuestButton);
log.info("should navigate to shipping page");
sa.assertTrue(gVar.assertNotVisible(shippingObjects.ShippingPage_Heading));
sa.assertAll();

}

@Test(groups="{Regression}",description="125619",dataProvider="LoginEmailValidation",dataProviderClass=Data.class)
public void TC12_logIn_Passwordvalidation(TestData l) throws Exception
{
	
	log.info("fetching data from data providers");
	l1.getWebElement("Login_Username", "Profile\\login.properties").sendKeys("Vinaylanka@gmail.com");
	l1.getWebElement("Login_Password", "Profile\\login.properties").sendKeys(l.get(0));
	log.info("click on login button");
	l1.getWebElement("Login_Login_Button", "Profile\\login.properties").click();
	l1.getWebElement("Login_Password", "Profile\\login.properties").clear();
	
	if(i==0 || i==1)
	{
		sa.assertEquals(GetData.getDataFromExcel("\\data\\ProfileData.xls","Passwordvalidation",1,0),l1.getWebElement("Login_Username", "Profile\\login.properties").getCssValue("background-color"));
		//Error Messages
		sa.assertEquals(GetData.getDataFromProperties("POM\\ErrorMessages\\ErrorMessages.properties","EmailId_Errormessage"),l1.getWebElement("Login_Password", "Profile\\login.properties").getText());
		
	}
	
	if(i==2)
	{
		//Error Messages
		sa.assertEquals(GetData.getDataFromProperties("POM\\ErrorMessages\\ErrorMessages.properties","Password_EmptyErrormessage"),l1.getWebElement("Login_Password", "Profile\\login.properties").getText());
	}
	
	if(i==3)
	{
		sa.assertEquals(GetData.getDataFromExcel("\\data\\ProfileData.xls","OrderIdValidation",1,1),l1.getWebElement("Login_Password", "Profile\\login.properties").getText().length());
	}
	else
	{
		log.info("Myaccount section page verificatrion");
		sa.assertTrue(l1.getWebElement("MyAccount_options","Profile\\MyAccountHome.properties").isDisplayed());
	}
}
/*
@Test(groups="{Regression}",description="124968")
public void TC01_LoginfromILP() throws Exception
{

log.info("navigate back to ILP page");
l1.getWebElement(ShippingPOM.Shippingpage_editlink);	
log.info("enter user name");
l1.getWebElement(CheckoutIlpPOM.Ilp_usernametextbox).clear();
l1.getWebElement(CheckoutIlpPOM.Ilp_usernametextbox).sendKeys("vla@dummy.com");
log.info("enter password");
l1.getWebElement(CheckoutIlpPOM.Ilp_passwordtbox).clear();
l1.getWebElement(CheckoutIlpPOM.Ilp_passwordtbox).sendKeys("Vin74113");
log.info("click on login button");
l1.getWebElement(CheckoutIlpPOM.Ilp_loginbutton).click();
log.info("should navigate to shipping page");
sa.assertTrue(gVar.assertNotVisible(ShippingPOM.ShippingPage_Heading));
sa.assertAll();

}

@Test(groups="{Regression}",description="288130")
public void TC03_ProductNameLinkLinkOrderSummary() throws Exception
{

Thread.sleep(3000);	
log.info("Edit link in order summary page");
sa.assertTrue(gVar.assertVisible(CheckoutIlpPOM.Ilp_ProductNameLink),"Edit link in ordersummary page");
log.info("Click on order summary product naem Link");
String ProductName=l1.getWebElement(CheckoutIlpPOM.Ilp_ProductNameLink).getText();
log.info("Click on order summary product naem Link click");
l1.getWebElement(CheckoutIlpPOM.Ilp_ProductNameLink).click();
log.info("Should navigate to PDP page");
sa.assertTrue(gVar.assertVisible("PDP_Product_Name","ShopNav\\PDP.properties"),"Edit link in ordersummary page");
log.info("Fetching heading P page");
String ProductNameinPDPage=l1.getWebElement("PDP_Product_Name","ShopNav\\PDP.properties").getText();
sa.assertEquals(ProductName,ProductNameinPDPage);
sa.assertAll();

}



@Test(groups="{Regression}",description="288128")
public void TC02_EditLinkOrderSummary() throws Exception
{

log.info("Edit link in order summary page");
sa.assertTrue(gVar.assertVisible(CheckoutIlpPOM.Ilp_EditLink_OrderSummary),"Edit link in ordersummary page");
log.info("Click on order summary page");
l1.getWebElement(CheckoutIlpPOM.Ilp_EditLink_OrderSummary).click();
log.info("Navigate to Cart page");
sa.assertTrue(gVar.assertVisible(CartPOM.cartContinueshoppingHeading),"Checkout As a guest button");
Thread.sleep(3000);
log.info("Click on checkout button in cart");
l1.getWebElements(CartPOM.cartCheckoutbuttons).get(0).click();
sa.assertAll();

}


@Test(groups="{Regression}",description="124971",dataProvider="LoginEmailValidation",dataProviderClass=Data.class)
public void TC11_logIn_EmailIdvalidation(TestData L) throws Exception
{
	p.ClearEmailId();
	log.info("fetching data from data providers");
	l1.getWebElement("Login_Username", "Profile\\login.properties").sendKeys(L.get(0));
	l1.getWebElement("Login_Password", "Profile\\login.properties").sendKeys("Vin@74113");
	log.info("click on login button");
	l1.getWebElement("Login_Login_Button", "Profile\\login.properties").click();

	
	
	if(i==0 || i==1 || i==2)
	{
		
		//Error Messages
		sa.assertEquals(l1.getWebElement("Login_Username_ErrorMsg", "Profile\\login.properties").getText(),GetData.getDataFromProperties("\\POM\\ErrorMessages\\ErrorMessages.properties","EmailId_Errormessage"));
		
	}

	else if(i==3)
	{
		//Error Messages
		sa.assertEquals(l1.getWebElement("Login_Username_ErrorMsg", "Profile\\login.properties").getText(),GetData.getDataFromProperties("\\POM\\ErrorMessages\\ErrorMessages.properties","EmailId_RequiredErrorMessage"));
	}
	

	else if(i==4 || i==5 || i==6 || i==7 || i==8)
		{
		//Error Messages
		sa.assertEquals(GetData.getDataFromProperties("\\POM\\ErrorMessages\\ErrorMessages.properties","EmailId_Errormessage"),l1.getWebElement("Login_Username_ErrorMsg", "Profile\\login.properties").getText());
	}
	
	else if(i==9 || i==10)
	{
		log.info("length Validation"+i);
		sa.assertEquals(GetData.getDataFromExcel("\\data\\ProfileData.xls","EmailValidation",1,1),l1.getWebElement("Login_Username", "Profile\\login.properties").getText().length());
		//Error Messages
		sa.assertEquals(l1.getWebElement("Login_Username_ErrorMsg", "Profile\\login.properties").getText(),GetData.getDataFromProperties("\\POM\\ErrorMessages\\ErrorMessages.properties","EmailId_Errormessage"));
	}
	
	else
	{
		log.info("Myaccount section page verificatrion");
		sa.assertTrue(l1.getWebElement("MyAccount_options","Profile\\MyAccountHome.properties").isDisplayed());
	}

	sa.assertAll();
}
	

@Test(groups="{Regression}",description="125619",dataProvider="LoginEmailValidation",dataProviderClass=Data.class)
public void TC12_logIn_Passwordvalidation(TestData l) throws Exception
{
	
	log.info("fetching data from data providers");
	l1.getWebElement("Login_Username", "Profile\\login.properties").sendKeys("Vinaylanka@gmail.com");
	l1.getWebElement("Login_Password", "Profile\\login.properties").sendKeys(l.get(0));
	log.info("click on login button");
	l1.getWebElement("Login_Login_Button", "Profile\\login.properties").click();
	l1.getWebElement("Login_Password", "Profile\\login.properties").clear();
	
	if(i==0 || i==1)
	{
		sa.assertEquals(GetData.getDataFromExcel("\\data\\ProfileData.xls","Passwordvalidation",1,0),l1.getWebElement("Login_Username", "Profile\\login.properties").getCssValue("background-color"));
		//Error Messages
		sa.assertEquals(GetData.getDataFromProperties("POM\\ErrorMessages\\ErrorMessages.properties","EmailId_Errormessage"),l1.getWebElement("Login_Password", "Profile\\login.properties").getText());
		
	}
	
	if(i==2)
	{
		//Error Messages
		sa.assertEquals(GetData.getDataFromProperties("POM\\ErrorMessages\\ErrorMessages.properties","Password_EmptyErrormessage"),l1.getWebElement("Login_Password", "Profile\\login.properties").getText());
	}
	
	if(i==3)
	{
		sa.assertEquals(GetData.getDataFromExcel("\\data\\ProfileData.xls","OrderIdValidation",1,1),l1.getWebElement("Login_Password", "Profile\\login.properties").getText().length());
	}
	else
	{
		log.info("Myaccount section page verificatrion");
		sa.assertTrue(l1.getWebElement("MyAccount_options","Profile\\MyAccountHome.properties").isDisplayed());
	}
}
*/

}
