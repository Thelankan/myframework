package com.cbk.checkout;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;

import com.cbk.pom.BillingPOM;
import com.cbk.pom.CartPOM;
import com.cbk.pom.CheckoutIlpPOM;
import com.cbk.pom.OrderConfirmationPOM;
import com.cbk.pom.OrderreviewPOM;
import com.cbk.pom.PaymentPOM;
import com.cbk.pom.ShippingPOM;
import com.cbk.utilgeneric.BaseTest;
import com.cbk.utilgeneric.GetData;
import com.cbk.utilgeneric.TestData;

public class Billing_Reg extends BaseTest {
	
String ProductNameinOrderSummarySection;
String ProductNameinCartPage;
String OS_Subtotal;
String OS_ShippingValue;
String OS_SalesTax;
String ShippingBillingbutton;
	
@Test(groups="{Regression}",description="148873")
public void TC00_usethisaddressforBilling() throws Exception
{
	
	log.info("Login");
	p.loginToAppliction(l1);
	log.info("Navigate to shipping page");
	checkout.NavigateToShippingPage();
	log.info("Entering shipping address");
	checkout.ShippingAddress("//data//Checkout.xls","ShippingAddress",1);
	log.info("Entering shipping address");
	gVar.SelectCheckbox(shippingObjects.ShippingPage_SameasBilling_Checkbox);
	log.info("Save address in profile");
	gVar.SelectCheckbox(shippingObjects.ShippingPage_AddtoAddressbook_Checkbox);
	log.info("Click on continue button");
	l1.getWebElements(shippingObjects.Shipping_ContinueToBilling_Button).get(0).click();
	log.info("Verifying the billing heading");
	l1.getWebElement(billingObjects.billingPage_Heading).isDisplayed();
	log.info("Verifying shipping and billing values");
	sa.assertEquals(gVar.assertEqual(billingObjects.billingPage_FN_TextBox),GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,1));
	sa.assertEquals(gVar.assertEqual(billingObjects.billingPage_LN_TextBox),GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,2));
	sa.assertEquals(gVar.assertEqual(billingObjects.billingPage_Address1_TextBox),GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,3));
	sa.assertEquals(gVar.assertEqual(billingObjects.billingPage_Address2_TextBox),GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,4));
	sa.assertEquals(gVar.assertEqual(billingObjects.billingPage_City_TextBox),GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,5));
	sa.assertEquals(gVar.assertEqual(billingObjects.billingPage_ZipCode_TextBox),GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,6));
	sa.assertEquals(gVar.assertEqual(billingObjects.billingPage_Country_Dropdown),GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,7));
	sa.assertEquals(gVar.assertEqual(billingObjects.billingPage_State_Dropdown),GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,8));
	sa.assertEquals(gVar.assertEqual(billingObjects.billingPage_Phone_TextBox),GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,9));

}

@Test(groups="{Regression}",description="148974")
public void TC01_EditlinkOrderReviewpage() throws Exception
{
	
	log.info("Fetching product name in order summary section shipping page");
	ProductNameinOrderSummarySection=l1.getWebElement(shippingObjects.Shipping_orderSummary_productName).getText();
	log.info("OCP Product Name"+ProductNameinOrderSummarySection);
	l1.getWebElement(billingObjects.billingPage_ordersummary_editlink).click();
	log.info("Should navigate to cart page");
	ProductNameinCartPage=l1.getWebElement(CartPOM.cartProductName).getText();
	sa.assertEquals(ProductNameinOrderSummarySection, ProductNameinCartPage);
	log.info("navigate back to shipping page");
	l1.getWebElement(CartPOM.cartCheckoutbuttonTop).click();
try {
		
		log.info("Clicking on checkout as a guest button");
		l1.getWebElement(CheckoutIlpPOM.Ilp_CheckoutasGuestButton).click();
		log.info("Shipping page Heading");
		l1.getWebElement(shippingObjects.ShippingPage_Heading).isDisplayed();
		
		
	} catch (Exception e) {
		
		log.info("Shipping page Heading");
		l1.getWebElement(shippingObjects.ShippingPage_Heading).isDisplayed();
		
	}

log.info("Click on continue button in shipping");
l1.getWebElements(shippingObjects.Shipping_ContinueToBilling_Button).get(0).click();
	

}
	
@Test(groups="{Regression}",description="128301")
public void TC00_navigatetoOrderReviewpage() throws Exception
{
	
	log.info("Enter email ID");
	l1.getWebElement(billingObjects.billingPage_Email_Textbox).sendKeys(GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,10));
	log.info("Navigate to shipping page");
	checkout.PaymentDetails("//data//Checkout.xls","PaymentDetails",1);
	log.info("click on continue button ib billing page");
	l1.getWebElement(PaymentPOM.Payment_PlaceOrderButton).click();
	try {
		
		sa.assertTrue(gVar.assertVisible(shippingObjects.shippingaddressValidationDialogue));
		l1.getWebElement(shippingObjects.shippingaddressValidationContinue).click();
		l1.getWebElement(OrderreviewPOM.OrderReview_placeOrderButton).isDisplayed();
		
	} catch (Exception e) {
		
		l1.getWebElement(OrderreviewPOM.OrderReview_placeOrderButton).isDisplayed();
		log.info("Navigated to billing page");
	}
	
	sa.assertAll();
	
}

@Test(groups="{Regression}",description="124446")
public void TC01_navigatetoOrderReviewpage() throws Exception
{
	
	log.info("Enter email ID in billing page");
	l1.getWebElement(billingObjects.billingPage_Email_Textbox).sendKeys(GetData.getDataFromExcel("//data//Checkout.xls","ShippingAddress",1,10));
	log.info("Entering payment details");
	checkout.PaymentDetails("//data//Checkout.xls","PaymentDetails",1);
	log.info("click on save card checkbox in payment page");
	gVar.SelectCheckbox(billingObjects.billingPage_paymentSaveCardCheckbox);
	log.info("click on continue button ib billing page");
	l1.getWebElement(PaymentPOM.Payment_PlaceOrderButton).click();
	
	try {
		
		sa.assertTrue(gVar.assertVisible(shippingObjects.shippingaddressValidationDialogue));
		l1.getWebElement(shippingObjects.shippingaddressValidationContinue).click();
		l1.getWebElement(OrderreviewPOM.OrderReview_placeOrderButton).isDisplayed();
		
	} catch (Exception e) {
		
		log.info("Navigated to billing page");
		l1.getWebElement(OrderreviewPOM.OrderReview_placeOrderButton).isDisplayed();
		
	}
	
	log.info("click on place order button");
	l1.getWebElement(OrderreviewPOM.OrderReview_placeOrderButton).click();
	
	log.info("Order confirmation heading");
	l1.getWebElement(OrderConfirmationPOM.orderconfirmation_Heading);
	log.info("Navigate to profile payment page click on my account icon");
	l1.getWebElement("MyAccountIcon","Profile\\MyAccountHome.properties").click();
	log.info("Click on payment icon in my account landing page");
	l1.getWebElements("PaymentSettings_Link_MyaccountLandingPage","Profile\\PaymentSettings.properties");
	log.info("Comparing card owners name");
	sa.assertEquals(l1.getWebElement("PaymentSettings_Ownername","Profile\\PaymentSettings.properties").getText(),GetData.getDataFromExcel("//data//Checkout.xls","PaymentDetails",1,1),"Card owner name");
	sa.assertEquals(l1.getWebElement("PaymentSettings_CardType","Profile\\PaymentSettings.properties").getText(),GetData.getDataFromExcel("//data//Checkout.xls","PaymentDetails",1,2),"Card type");
	sa.assertEquals(l1.getWebElement("PaymentSettings_CardNumber","Profile\\PaymentSettings.properties").getText(),GetData.getDataFromExcel("//data//Checkout.xls","PaymentDetails",1,3),"Card Number");
	sa.assertEquals(l1.getWebElement("PaymentSettings_Expiredate","Profile\\PaymentSettings.properties").getText(),GetData.getDataFromExcel("//data//Checkout.xls","PaymentDetails",1,4),"Expiredate");
	sa.assertTrue(gVar.assertVisible("PaymentSettings_DeleteCard","Profile\\PaymentSettings.properties"));
	
	sa.assertAll();

}


@Test(groups="{Regression}",description="124437")
public void TC00_saveaddresstoaddressbook() throws Exception
{
	
	log.info("Navigate to profile payment page click on my account icon");
	l1.getWebElement("MyAccountIcon","Profile\\MyAccountHome.properties").click();
	log.info("Click on Address link");
	l1.getWebElement("Address_Link_AccountLandingPage","Profile\\Addresses.properties").click();
	log.info("Delete address");
	p.DeleteAddress();
	log.info("Navigate to shipping page");
	checkout.NavigateToBillingPage();
	log.info("Enter billing address");
	checkout.BillingAddress("//data//Checkout.xls","BillingAddress",1);
	log.info("click onAdd to address book");
	gVar.SelectCheckbox(billingObjects.billingPage_addtoaddress_checkbox);
	log.info("Entering payment details");
	checkout.PaymentDetails("//data//Checkout.xls","PaymentDetails",1);
	log.info("click on continue button ib billing page");
	l1.getWebElement(PaymentPOM.Payment_PlaceOrderButton).click();
	log.info("click on place order button in order review page");
	l1.getWebElement(OrderreviewPOM.OrderReview_placeOrderButton).click();
	log.info("Order confirmation page heading");
	l1.getWebElement(OrderConfirmationPOM.orderconfirmation_Heading).click();
	log.info("Navigate to profile payment page click on my account icon");
	l1.getWebElement("MyAccountIcon","Profile\\MyAccountHome.properties").click();
	log.info("Click on Address link");
	l1.getWebElement("Address_Link_AccountLandingPage","Profile\\Addresses.properties").click();
	sa.assertEquals(gVar.assertEqual("Address_DefaultAddress", "Profile\\Addresses.properties").toString(), GetData.getDataFromExcel("\\data\\Checkout.xls","BillingAddress",1,12));
	
	sa.assertAll();
	
}


@Test(groups="{Regression}",description="148884")
public void TC00_Privacypolicylink() throws Exception
{
	
log.info("click on Privacy Policy Button");
l1.getWebElement(billingObjects.billingPage_PrivacyPolicy).click();
log.info("Privacy Policy Overlay");
sa.assertTrue(l1.getWebElement(billingObjects.billingPagePrivacyPolicy_Overlay).isDisplayed());
log.info("Privacy Policy heading");
sa.assertTrue(l1.getWebElement(billingObjects.billingPage_PrivacyPolicy_heading).isDisplayed());
log.info("Register Privacy Policy overlay close button");
l1.getWebElement(billingObjects.billingPage_PrivacyPolicy_CloseButton).click();
log.info("navigate back to billing page");
sa.assertAll();
	
}


@Test
public void TC00_AddCardDetails() throws Exception
{
       
/*Actions actions=new Actions(driver); 
driver.get("https://dev.christopherandbanks.com/missy-4-16/");
driver.findElement(By.xpath("//span[@class='show-desktop']")).click();
actions.click().build().perform();
driver.findElement(By.xpath("//div[@class='login-box login-account']//input[(contains(@id,'dwfrm_login_username'))]")).sendKeys("username@xmail.com");
driver.findElement(By.xpath("//div[@class='login-box login-account']//input[(contains(@id,'dwfrm_login_password'))]")).sendKeys("Password3rK");
driver.findElement(By.xpath("//button[@class='spc-login-btn']")).click();*/
       
p.loginToAppliction(l1);
Thread.sleep(5000);

//click on payment method button
driver.findElement(By.xpath("//a[@class='nav-paymentmethod']")).click();
Thread.sleep(3000);
p.DeleteCreditCard();
Thread.sleep(3000);
for(int i=1;i<=4;i++)
{
//click on add credit card button
driver.findElement(By.xpath("//a[@class='section-header-note add-card']")).click(); 
Thread.sleep(2000);
log.info("clicked on add credit card");
WebElement payment_dropdown=driver.findElement(By.xpath("//div[@class='custom-select']//select[@id='dwfrm_paymentinstruments_creditcards_newcreditcard_type']")); 
Select payment=new Select(payment_dropdown);
driver.findElement(By.xpath("//div[@class='custom-select']//select[@id='dwfrm_paymentinstruments_creditcards_newcreditcard_type']")).click();
log.info("clicked on drop-down");
//Select dropdown=new Select(driver.findElement(By.xpath("//select[@id='dwfrm_paymentinstruments_creditcards_newcreditcard_type']//option[@class='select-option']")));
//List<WebElement> list=dropdown.getOptions();
//System.out.println("the type of cards are"+list);
    payment.selectByIndex(i);
    WebElement selected1=payment.getFirstSelectedOption();
    System.out.println(selected1.getText());
  //passing value from excel sheet
driver.findElement(By.xpath("//div[@class='paymentfields']//input[@id='dwfrm_paymentinstruments_creditcards_newcreditcard_owner']")).sendKeys(GetData.getDataFromExcel("//data//Checkout.xls","PaymentDetails",i,1));   
//checkout.PaymentDetails("//data//Checkout.xls","PaymentDetails",1);
driver.findElement(By.xpath("//div[@class='paymentfields']//input[contains(@id,'dwfrm_paymentinstruments_creditcards_newcreditcard_number')]")).sendKeys(GetData.getDataFromExcel("//data//Checkout.xls","PaymentDetails",i,3));

//select month from month drop-down
WebElement month_dropdown=driver.findElement(By.xpath("//div[@class='paymentfields']//select[@id='dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_month']")); 
Select month=new Select(month_dropdown);
driver.findElement(By.xpath("//div[@class='paymentfields']//select[@id='dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_month']")).click();
month.selectByIndex(2);

//select year from day drop down
WebElement year_dropdown=driver.findElement(By.xpath("//div[@class='paymentfields']//select[@id='dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_year']")); 
Select year=new Select(year_dropdown);
driver.findElement(By.xpath("//div[@class='paymentfields']//select[@id='dwfrm_paymentinstruments_creditcards_newcreditcard_expiration_year']")).click();
year.selectByIndex(4);

driver.findElement(By.xpath("//button[@id='applyBtn']")).click();
    Thread.sleep(3000);
    }

sa.assertAll();
    
}

@Test
public void TC01_NavigatetoBillingpageandplaceorder() throws Exception
{
       
       checkout.NavigateToShippingPage();
       log.info("Enter shipping address");
       checkout.ShippingAddress("//data//Checkout.xls","ShippingAddress",1);
       log.info("Entering shipping address");
       gVar.SelectCheckbox(shippingObjects.ShippingPage_SameasBilling_Checkbox);
       log.info("Navigate to payment page");
       l1.getWebElement(billingObjects.billingPage_continue_button).click();
       
       //select drop down place order
       
       
       log.info("Click on continue to order details page");
       l1.getWebElement(PaymentPOM.Payment_PlaceOrderButton).click();
       l1.getWebElement(OrderreviewPOM.OrderReview_placeOrderButton).click();
       
       sa.assertAll();
       
}
       
       

}

