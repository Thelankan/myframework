package com.cbk.shopnav;

import org.testng.annotations.Test;
import org.testng.xml.XmlTest;

import com.cbk.pom.HeaderPOM;
import com.cbk.utilgeneric.BaseTest;
import com.cbk.utilgeneric.GetData;

public class Header extends BaseTest
{
	
//@@@@@@@@@@@@@@@@@@@@@ For Guest User @@@@@@@@@@@@@@@@@@@@@
	
@Test(groups="{Regression}",description="206608,206620,207690")
public void TC01_HeaderUIelementsforGuestrUser() throws Exception
{
	
	log.info("Promo bar section");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerPromobar),"header promo slot");
	log.info("Header login link");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerLoginLink),"header Login Link");
	log.info("Header stores link");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerStoreslink),"header Stores Link");
	log.info("Header credit card link");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerCreditcardlink),"header credit card Link");
	log.info("Header social link");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerSociallink),"header social Link");
	log.info("Header headerorderstatuslink");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerorderstatuslink),"header headerorderstatuslink");
	log.info("Header headerhelplink");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerhelplink),"header headerhelplink");
	log.info("Header headerSearchtextbox");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerSearchtextbox),"header headerSearchtextbox");
	log.info("Header headerHomepagelogo");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerHomepagelogo),"header headerHomepagelogo");
	log.info("Header headerMinicartEmpty");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerMinicartEmpty),"header headerMinicartEmpty");
	log.info("Header headerMinicartQuantity");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerMinicartQuantity),"header headerMinicartQuantity");
	
	log.info("click on Header login link");
	l1.getWebElement(HeaderPOM.headerLoginLink).click();
	log.info("Should navigate to my account login page");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerMyaccountHeading),"header headerMinicartQuantity");
	
	sa.assertAll();
	
}

	
@Test(groups="{Regression}",description="206610")
public void TC02_FunctionalityofBagIcon() throws Exception
{
	
	log.info("click on Header mini cart quantity");
	l1.getWebElement(HeaderPOM.headerMinicartQuantity).click();
	log.info("mini cart empty heading");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerMinicartemptyHeading),"header headerMinicartQuantity");
    sa.assertAll();
    
}

@Test(groups="{Regression}",description="206611,206612")
public void TC03_FunctionalityofHeaderDropdown() throws Exception
{
	
	log.info("click on + icon in header");
	l1.getWebElement(HeaderPOM.headerDropdownSymboolPlus).click();
	log.info("Promo details overlay should display");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerPromodetailsoverlay),"Promo details overlay should display");
	
}

@Test(groups="{Regression}",description="206613")
public void TC04_MouseHoveronloginLink() throws Exception
{
	
	log.info("mouse hover on login link");
	act.moveToElement(l1.getWebElement(HeaderPOM.headerLoginLink));
	log.info("Login form should display");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerLoginform),"Promo details overlay should display");

}

@Test(groups="{Regression}",description="206614")
public void TC05_StoreslinkFunctionality() throws Exception
{
	
	log.info("Stores link functionality");
	l1.getWebElement(HeaderPOM.headerStoreslink).click();
	log.info("Should navigate to store locator page");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerStorelocatorHeading),"Store locator heading");
	
}

@Test(groups="{Regression}",description="206615,206616,206617,206618,")
public void TC06_headeruppernavlinks() throws Exception
{
	
	log.info("Stores link functionality");
	int uppernavlinkssize=l1.getWebElements(HeaderPOM.headeruppernavlinks).size();
	log.info(uppernavlinkssize);
	
	for(int i=0;i<uppernavlinkssize;i++)
	{
	
		log.info("click on all links in header nav");
		l1.getWebElements(HeaderPOM.headeruppernavlinks).get(i).click();
		log.info("verification of the pages");
		
		if(i==0){
			sa.assertTrue(gVar.assertVisible(HeaderPOM.headercreditheading),"Heading credit card");
		}
		else if (i==1){
			sa.assertTrue(gVar.assertVisible(HeaderPOM.headerSocialLinkwrapper),"Social links");
		}
		else if (i==2){
			sa.assertTrue(gVar.assertVisible(HeaderPOM.headerViewOrderHistory),"Social links");
		}
		else if (i==3){
			sa.assertTrue(gVar.assertVisible(HeaderPOM.headerViewOrderHistory),"Social links");
		}
		
		log.info("click on home page logo");
		l1.getWebElement(HeaderPOM.headerHomepagelogo).click();
		
	}
	
	log.info("Should navigate to store locator page");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerStorelocatorHeading),"Store locator heading");
	
}

@Test(groups="{Regression}",description="206621,206622")
public void TC07_SearchboxHeader() throws Exception
{

	log.info("Search textbox visibility");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerSearchtextbox));
	log.info("enter text in Search textbox");
	l1.getWebElement(HeaderPOM.headerSearchtextbox).sendKeys("040040600003125");
	log.info("Get text of search textbox");
	String searchText=l1.getWebElement(HeaderPOM.headerSearchtextbox).getText();
	sa.assertEquals(searchText,"040040600003125");
	
}

@Test(groups="{Regression}",description="206621,206622")
public void TC08_HeaderCategoryLinks() throws Exception
{

	int CategoryLinksSize=l1.getWebElements(HeaderPOM.headerCategoryLinks).size();
	
	for(i=0;i<CategoryLinksSize;i++)
	{
		
		log.info("click on each and every link in category section");
		l1.getWebElements(HeaderPOM.headerCategoryLinks).get(i).click();
		log.info("should navigate to particular page");
		l1.getWebElement(HeaderPOM.headerBreadcrumbelement).getText();
		log.info("click on home page logo");
		l1.getWebElement(HeaderPOM.headerHomepagelogo).click();
		
	}
	
}

	
//@@@@@@@@@@@@@@@@@@@@@  For Register User @@@@@@@@@@@@@@@@@@@@@

@Test(groups="{Regression}",description="206609")
public void TC09_HeaderUIelementsforRegisterUser(XmlTest xmltest) throws Exception
{
	
	log.info("Login");
	p.loginToAppliction(l1);
	log.info("Promo bar section");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerPromobar),"header promo slot");
	log.info("Header login link");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerLoginLink),"header Login Link");
	log.info("Header stores link");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerStoreslink),"header Stores Link");
	log.info("Header credit card link");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerCreditcardlink),"header credit card Link");
	log.info("Header social link");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerSociallink),"header social Link");
	log.info("Header headerorderstatuslink");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerorderstatuslink),"header headerorderstatuslink");
	log.info("Header headerhelplink");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerhelplink),"header headerhelplink");
	log.info("Header headerSearchtextbox");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerSearchtextbox),"header headerSearchtextbox");
	log.info("Header headerHomepagelogo");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerHomepagelogo),"header headerHomepagelogo");
	log.info("Header headerMinicartEmpty");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerMinicartEmpty),"header headerMinicartEmpty");
	log.info("Header headerMinicartQuantity");
	sa.assertTrue(gVar.assertVisible(HeaderPOM.headerMinicartQuantity),"header headerMinicartQuantity");
	sa.assertAll();
	
}


	
}
