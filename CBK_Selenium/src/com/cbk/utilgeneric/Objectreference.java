package com.cbk.utilgeneric;

import org.testng.xml.XmlTest;

import com.cbk.pom.BillingPOM;
import com.cbk.pom.CartPOM;
import com.cbk.pom.CheckoutIlpPOM;
import com.cbk.pom.FooterPOM;
import com.cbk.pom.HeaderPOM;
import com.cbk.pom.LoginPom;
import com.cbk.pom.MiniCartPOM;
import com.cbk.pom.OrderConfirmationPOM;
import com.cbk.pom.OrderreviewPOM;
import com.cbk.pom.PaymentPOM;
import com.cbk.pom.RegisterPOM;
import com.cbk.pom.ShippingPOM;
import com.cbk.projectspec.CartFunctions;
import com.cbk.projectspec.CheckoutFunctions;
import com.cbk.projectspec.GlobalFunctions;
import com.cbk.projectspec.ProfileFunctions;
import com.cbk.projectspec.ShopnavFunctions;

public class Objectreference {
	
	public BillingPOM billingObjects;
	public ShippingPOM shippingObjects;
	public CartPOM cartObjects;
	public CheckoutIlpPOM checkoutilpObjects;
	public FooterPOM footerObjects;
	public HeaderPOM headerObjects;
	public LoginPom loginObjects;
	public MiniCartPOM minicartObjects;
	public OrderConfirmationPOM orderconfirmationObjects;
	public OrderreviewPOM orderreviewObjects;
	public PaymentPOM paymentObjects;
	public RegisterPOM registerObjects;
	public GetData GetData;
	public ProfileFunctions p;
	public ShopnavFunctions s;
	public CartFunctions cart;
	public CheckoutFunctions checkout;
	public GlobalFunctions gVar;
	public XmlTest xmlTest;
	
	
}
