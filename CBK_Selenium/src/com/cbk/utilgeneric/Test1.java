package com.cbk.utilgeneric;

/*import java.io.File;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import org.testng.xml.XmlTest;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;


public class Test1 extends BaseTest{

	@Test()
	public void ExtentTest000() throws Exception
	{
		//l1.getWebElement("signIn", "login.properties").click();
		SoftAssert sa=new SoftAssert();
		//sa.assertTrue(l1.getWebElement("loginReturnHeading", "login.properties").isDisplayed());
		sa.assertTrue(l1.getWebElement("Login_Heading", "Profile\\login.properties").isDisplayed());
		//sa.assertTrue(l1.getWebElement("emailLabel", "login.properties").isDisplayed());
		//sa.assertTrue(l1.getWebElement("emailTextBox", "login.properties").isDisplayed());
		//sa.assertTrue(l1.getWebElement("pwdLabel", "login.properties").isDisplayed());
		//sa.assertTrue(l1.getWebElement("pwdTextBox", "login.properties").isDisplayed());
		//sa.assertTrue(l1.getWebElement("submitBtn", "login.properties").isDisplayed());
		sa.assertAll();
	}
	@Test
	public void ExtentTest001() throws Exception
	{
		//l1.getWebElement("signIn", "login.properties").click();
		SoftAssert sa=new SoftAssert();
		//sa.assertTrue(l1.getWebElement("loginReturnHeading", "login.properties").isDisplayed());
		sa.assertTrue(l1.getWebElement("Login_Heading", "Profile\\login.properties").isDisplayed());
		//sa.assertTrue(l1.getWebElement("emailLabel", "login.properties").isDisplayed());
		//sa.assertTrue(l1.getWebElement("emailTextBox", "login.properties").isDisplayed());
		//sa.assertTrue(l1.getWebElement("pwdLabel", "login.properties").isDisplayed());
		//sa.assertTrue(l1.getWebElement("pwdTextBox", "login.properties").isDisplayed());
		//sa.assertTrue(l1.getWebElement("submitBtn", "login.properties").isDisplayed());
		sa.assertAll();
	}
	@Test
	public void ExtentTest002() throws Exception
	{
		//l1.getWebElement("signIn", "login.properties").click();
		SoftAssert sa=new SoftAssert();
		//sa.assertTrue(l1.getWebElement("loginReturnHeading", "login.properties").isDisplayed());
		sa.assertTrue(l1.getWebElement("Register_privacyPolicy_Link", "Profile\\Register.properties").isDisplayed());
		//sa.assertTrue(l1.getWebElement("emailLabel", "login.properties").isDisplayed());
		//sa.assertTrue(l1.getWebElement("emailTextBox", "login.properties").isDisplayed());
		//sa.assertTrue(l1.getWebElement("pwdLabel", "login.properties").isDisplayed());
		//sa.assertTrue(l1.getWebElement("pwdTextBox", "login.properties").isDisplayed());
		//sa.assertTrue(l1.getWebElement("submitBtn", "login.properties").isDisplayed());
		sa.assertAll();
	}

}
*/


import java.util.HashMap;
import java.util.Map;
import java.util.Set;
 
public class Test1 {
 
    public void findDuplicateChars(String str){
         
        Map<Character, Integer> dupMap = new HashMap<Character, Integer>(); 
        char[] chrs = str.toCharArray();
        for(Character ch:chrs){
            if(dupMap.containsKey(ch)){
                dupMap.put(ch, dupMap.get(ch)+1);
            } else {
                dupMap.put(ch, 1);
            }
        }
        Set<Character> keys = dupMap.keySet();
        for(Character ch:keys){
            if(dupMap.get(ch) > 1){
                System.out.println(ch+"--->"+dupMap.get(ch));
            }
        }
    }
     
    public static void main(String a[]){
//        DuplicateCharsInString dcs = new DuplicateCharsInString();
//        dcs.findDuplicateChars("Java2Novice");
    }
}
