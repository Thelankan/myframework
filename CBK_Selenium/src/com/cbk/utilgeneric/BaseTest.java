package com.cbk.utilgeneric;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.asserts.SoftAssert;
import org.testng.xml.XmlTest;

import com.cbk.pom.LoginPom;
import com.cbk.pom.RegisterPOM;
import com.cbk.projectspec.CartFunctions;
import com.cbk.projectspec.CheckoutFunctions;
import com.cbk.projectspec.GlobalFunctions;
import com.cbk.projectspec.ProfileFunctions;
import com.cbk.projectspec.ShopnavFunctions;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;


public class BaseTest extends Objectreference{

	public WebDriver driver;
	public Locators l1;
	public String c;

	public ExtentReports extentReportIE;
	public ExtentReports extentReportIE11;
	public ExtentReports extentReportEdge;
	public ExtentReports extentReportSafari;
	public ExtentReports extentReportFF;
	public ExtentReports extentReportChrome;
	public ExtentReports extentReportAndroidPhone;
	public ExtentReports extentReportAndroidTab;
	public ExtentReports extentReportiPhone;
	public ExtentReports extentReportiPad;
	public ExtentTest extentTestIE;
	public ExtentTest extentTestIE11;
	public ExtentTest extentTestEdge;
	public ExtentTest extentTestSafari;
	public ExtentTest extentTestFF;
	public ExtentTest extentTestChrome;
	public ExtentTest extentTestAndroidPhone;
	public ExtentTest extentTestAndroidTab;
	public ExtentTest extentTestiPhone;
	public ExtentTest extentTestiPad;
	public ExtentTest extentTestChildIE;
	public ExtentTest extentTestChildIE11;
	public ExtentTest extentTestChildEdge;
	public ExtentTest extentTestChildSafari;
	public ExtentTest extentTestChildFF;
	public ExtentTest extentTestChildChrome;
	public ExtentTest extentTestChildAndroidPhone;
	public ExtentTest extentTestChildAndroidTab;
	public ExtentTest extentTestChildiPhone;
	public ExtentTest extentTestChildiPad;
	public Logger log;
	public SoftAssert sa;
	public Actions act;
	public int i;
	
	
	public ProfileFunctions p;
	public ShopnavFunctions s;
	public CartFunctions cart;
	public CheckoutFunctions checkout;
	public GlobalFunctions gVar;

	
	//public XmlTest xmlTest;
	
@BeforeSuite
public void cleanUp(XmlTest xmlTest)
{
	//delete log directory
	try
	{
	FileUtils.deleteDirectory(new File(System.getProperty("user.dir")+"//log"));
	}
	catch(Exception e)
	{
		e.printStackTrace();
		System.out.println("Leave it");
	}
	//xmlTest=xmlTest;
}
	
	@Parameters({"broName","GridExecution"})
	@BeforeClass
	public void initialize(String browsername,String gridexecution) throws Exception
	{
		//BaseTest.xmlTest=xmlTest;
		if(browsername.equalsIgnoreCase("ie")) {
			//Report declaration code 
			extentReportIE = new ExtentReports("\\CbkReports\\MyReportIE.html", false);
			extentReportIE.loadConfig(new File(System.getProperty("user.dir")+"\\ExtentReports\\extent-config.xml"));
			extentTestIE=extentReportIE.startTest(this.getClass().getSimpleName());
			
			} else if(browsername.equalsIgnoreCase("firefox")) {
			extentReportFF = new ExtentReports("\\CbkReports\\MyReportFF.html", false);
			extentReportFF.loadConfig(new File(System.getProperty("user.dir")+"\\ExtentReports\\extent-config.xml"));
			extentTestFF=extentReportFF.startTest(this.getClass().getSimpleName());
			
			}else if(browsername.equalsIgnoreCase("chrome")) {
				extentReportChrome = new ExtentReports("\\CbkReports\\MyReportChrome.html", false);
				extentReportChrome.loadConfig(new File(System.getProperty("user.dir")+"\\ExtentReports\\extent-config.xml"));
				extentTestChrome=extentReportChrome.startTest(this.getClass().getSimpleName());
				
			} else if(browsername.equalsIgnoreCase("Edge")) {
				extentReportEdge = new ExtentReports("\\CbkReports\\MyReportEdge.html", false);
				extentReportEdge.loadConfig(new File(System.getProperty("user.dir")+"\\ExtentReports\\extent-config.xml"));
				extentTestEdge=extentReportEdge.startTest(this.getClass().getSimpleName());
				
			} else if(browsername.equalsIgnoreCase("Safari")) {
				extentReportSafari = new ExtentReports("\\CbkReports\\MyReportSafari.html", false);
				extentReportSafari.loadConfig(new File(System.getProperty("user.dir")+"\\ExtentReports\\extent-config.xml"));
				extentTestSafari=extentReportSafari.startTest(this.getClass().getSimpleName());
				
			} else if(browsername.equalsIgnoreCase("Android Phone")) {
				extentReportAndroidPhone = new ExtentReports("\\CbkReports\\MyReportAP.html", false);
				extentReportAndroidPhone.loadConfig(new File(System.getProperty("user.dir")+"\\ExtentReports\\extent-config.xml"));
				extentTestAndroidPhone=extentReportAndroidPhone.startTest(this.getClass().getSimpleName());
				
			} else if(browsername.equalsIgnoreCase("Android Tab")) {
				extentReportAndroidTab = new ExtentReports("\\CbkReports\\MyReportAT.html", false);
				extentReportAndroidTab.loadConfig(new File(System.getProperty("user.dir")+"\\ExtentReports\\extent-config.xml"));
				extentTestAndroidTab=extentReportAndroidTab.startTest(this.getClass().getSimpleName());
				
				
			} else if(browsername.equalsIgnoreCase("iPhone")) {
				extentReportiPhone = new ExtentReports("\\CbkReports\\MyReportiPhone.html", false);
				extentReportiPhone.loadConfig(new File(System.getProperty("user.dir")+"\\ExtentReports\\extent-config.xml"));
				extentTestiPhone=extentReportiPhone.startTest(this.getClass().getSimpleName());
				
			} else if(browsername.equalsIgnoreCase("iPad")) {
				extentReportiPad = new ExtentReports("\\CbkReports\\MyReportiPad.html", false);
				extentReportiPad.loadConfig(new File(System.getProperty("user.dir")+"\\ExtentReports\\extent-config.xml"));
				extentTestiPad=extentReportiPad.startTest(this.getClass().getSimpleName());
			}
		
		
		//driver.manage().window().maximize();
		
		driver=new GetDriver().getDriver(browsername,gridexecution);
		gVar=new GlobalFunctions(driver);
		gVar.navigateToSite(browsername);
		
		Dimension screenSize = new Dimension(1600, 900);
        driver.manage().window().setSize(screenSize);
        driver.manage().window().maximize();
		
/*		if(browsername.equalsIgnoreCase("firefox")) {
		driver.manage().window().setSize(new Dimension(1920, 1080));
	}
	else {
		Dimension screenSize = new Dimension(900, 900);
        driver.manage().window().setSize(screenSize);
        driver.manage().window().maximize();
	}*/
		
		
		
		log=Logger.getLogger(this.getClass().getName());
		act=new Actions(driver);
	}
	
	@Parameters("broName")
	@BeforeMethod
	public void extReport(Method m,String browsername)
	{
		
		if(browsername.equalsIgnoreCase("ie")) {			
			extentTestChildIE=extentReportIE.startTest(m.getName()+" "+driver.getClass().getSimpleName());
			extentTestIE.appendChild(extentTestChildIE);
			l1=new Locators(driver,extentTestChildIE,browsername);
			
		} else if(browsername.equalsIgnoreCase("firefox")) {			
			extentTestChildFF=extentReportFF.startTest(m.getName()+" "+driver.getClass().getSimpleName());
			extentTestFF.appendChild(extentTestChildFF);
			l1=new Locators(driver,extentTestChildFF,browsername);
			
		} else if(browsername.equalsIgnoreCase("chrome")) {					
			extentTestChildChrome=extentReportChrome.startTest(m.getName()+" "+driver.getClass().getSimpleName());
			extentTestChrome.appendChild(extentTestChildChrome);
			l1=new Locators(driver,extentTestChildChrome,browsername);
					
		} else if(browsername.equalsIgnoreCase("Edge")) {					
			extentTestChildEdge=extentReportEdge.startTest(m.getName()+" "+driver.getClass().getSimpleName());
			extentTestEdge.appendChild(extentTestChildEdge);
			l1=new Locators(driver,extentTestChildEdge,browsername);
					
		} else if(browsername.equalsIgnoreCase("Safari")) {					
			extentTestChildSafari=extentReportSafari.startTest(m.getName()+" "+driver.getClass().getSimpleName());
			extentTestSafari.appendChild(extentTestChildSafari);
			l1=new Locators(driver,extentTestChildSafari,browsername);
					
		} else if(browsername.equalsIgnoreCase("Android Phone")) {			
			extentTestChildAndroidPhone=extentReportAndroidPhone.startTest(m.getName()+" "+driver.getClass().getSimpleName());
			extentTestAndroidPhone.appendChild(extentTestChildAndroidPhone);
			l1=new Locators(driver, extentTestChildAndroidPhone,browsername);
			
		} else if(browsername.equalsIgnoreCase("Android Tab")) {
			extentTestChildAndroidTab=extentReportAndroidTab.startTest(m.getName()+" "+driver.getClass().getSimpleName());
			extentTestAndroidTab.appendChild(extentTestChildAndroidTab);
			l1=new Locators(driver, extentTestChildAndroidTab,browsername);
			
		} else if(browsername.equalsIgnoreCase("iPhone")) {
			extentTestChildiPhone=extentReportiPhone.startTest(m.getName()+" "+driver.getClass().getSimpleName());
			extentTestiPhone.appendChild(extentTestChildiPhone);
			l1=new Locators(driver, extentTestChildiPhone,browsername);
			
		} else if(browsername.equalsIgnoreCase("iPad")) {
			extentTestChildiPad=extentReportiPad.startTest(m.getName()+" "+driver.getClass().getSimpleName());
			extentTestiPad.appendChild(extentTestChildiPad);
			l1=new Locators(driver, extentTestChildiPad,browsername);
			
		}
		sa=new SoftAssert();
		log.info("Test Case "+m.getName()+"Started in "+driver.getClass().getSimpleName()+"driver");
		log.info("-------------------------------------------");
		p=new ProfileFunctions(driver);
		s=new ShopnavFunctions();
		cart = new CartFunctions();
		checkout=new CheckoutFunctions();
		loginObjects=new LoginPom();
		registerObjects=new RegisterPOM();  
		
		System.out.println("################# Befroe Method ###################");

	}
	
	@AfterMethod
	public void endTest(Method m)
	{
		log.info("Test Case "+m.getName()+"ended in "+driver.getClass().getSimpleName()+"driver");
		log.info("-------------------------------------------");
/*		if(browsername.equalsIgnoreCase("ie")) {
			
			extentReportIE.endTest(extentTestIE);
			
		} else if(browsername.equalsIgnoreCase("firefox")) {
			
			extentReportFF.endTest(extentTestFF);
			
		} else if(browsername.equalsIgnoreCase("Android Phone")) {
			
			extentReportAndroidPhone.endTest(extentTestAndroidPhone);
		}*/
	}
	
	@Parameters("broName")
	@AfterTest
	public void endTest(String browsername)
	{
		if(browsername.equalsIgnoreCase("ie")) {
			//extentTestIE.appendChild(extentTestChildIE);
			extentReportIE.endTest(extentTestIE);
			extentReportIE.flush();
			
		} else if(browsername.equalsIgnoreCase("firefox")) {
			//extentTestFF.appendChild(extentTestChildFF);
			extentReportFF.endTest(extentTestFF);
			extentReportFF.flush();
			
		} else if(browsername.equalsIgnoreCase("Chrome")) {
			//extentTestChrome.appendChild(extentTestChildChrome);
			extentReportChrome.endTest(extentTestChrome);
			extentReportChrome.flush();
			
		} else if(browsername.equalsIgnoreCase("Edge")) {
			//extentTestEdge.appendChild(extentTestChildEdge);
			extentReportEdge.endTest(extentTestEdge);
			extentReportEdge.flush();
			
		} else if(browsername.equalsIgnoreCase("Safari")) {
			//extentTestSafari.appendChild(extentTestChildSafari);
			extentReportSafari.endTest(extentTestSafari);
			extentReportSafari.flush();
			
		} else if(browsername.equalsIgnoreCase("Android Phone")) {
			//extentTestAndroidPhone.appendChild(extentTestChildAndroidPhone);
			extentReportAndroidPhone.endTest(extentTestAndroidPhone);
			extentReportAndroidPhone.flush();
			
		} else if(browsername.equalsIgnoreCase("Android Tab")) {
			//extentTestAndroidPhone.appendChild(extentTestChildAndroidPhone);
			extentReportAndroidTab.endTest(extentTestAndroidTab);
			extentReportAndroidTab.flush();
			
		} else if(browsername.equalsIgnoreCase("iPhone")) {
			//extentTestAndroidPhone.appendChild(extentTestChildAndroidPhone);
			extentReportiPhone.endTest(extentTestiPhone);
			extentReportiPhone.flush();
			
		} else if(browsername.equalsIgnoreCase("iPad")) {
			//extentTestAndroidPhone.appendChild(extentTestChildAndroidPhone);
			extentReportiPad.endTest(extentTestiPad);
			extentReportiPad.flush();
		}
	}
	
	@AfterSuite
	public void tearDown(XmlTest xmlTest) throws IOException
	{
		
		new GetDriver().closeDriver();
		//move log files
		String str=new Date().getDate()+" "+new Date().getMonth()+" "+new Date().getYear()+" "+new Date().getHours()+" "+new Date().getMinutes()+" "+new Date().getSeconds();
		System.out.println("str"+str);
		new File("C:\\Prevail Reports\\HTML\\Results "+str).mkdirs();
		File source = new File(System.getProperty("user.dir")+"\\log");
		File dest = new File("C:\\Prevail Reports\\HTML\\Results "+str);
		try {
		  FileUtils.copyDirectory(source, dest);
		} catch (IOException e) {
		  e.printStackTrace();
		}
/*
		try {
			Process p=Runtime.getRuntime().exec("cmd /c start "+System.getProperty("user.dir")+"\\Sel_Report.bat");
			p.waitFor();
		} catch (IOException e) {
			System.out.println("catch block");
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}*/

	}
	
	@DataProvider
	public Object[][] PassData()
	{
		Object[][] data = new Object[3][2];
		
		return data;
	}
	
}
