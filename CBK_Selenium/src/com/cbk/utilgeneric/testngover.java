package com.cbk.utilgeneric;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class testngover {
	
	@BeforeMethod
	public void method1(){
		
		System.out.println("Before method one");		
	}

}

class test23 extends testngover{
	
	@Override
	@BeforeMethod
	public void method1(){
		System.out.println("before method two overrided");
	}
	
	@Test
	public void testmethod(){
		System.out.println("testing one");
	}
	
	@Test
	public void testmethod1(){
		System.out.println("testing two");
	}
	
	@AfterMethod
	public void aftermethod(){
		System.out.println("After method");
	}
	
}
