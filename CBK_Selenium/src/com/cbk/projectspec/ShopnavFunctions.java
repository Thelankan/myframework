package com.cbk.projectspec;

import org.openqa.selenium.WebDriver;
import org.testng.asserts.SoftAssert;

import com.cbk.utilgeneric.BaseTest;
import com.cbk.utilgeneric.Locators;

public class ShopnavFunctions extends BaseTest{

	public String PDP_Price;
	public BaseTest BaseTest;
	
public void NavigateToCategoryPage() throws Exception{
		BaseTest.l1.getWebElements("RootCategory_Links","ShopNav\\CategoryLanding.properties").get(1).click();
		BaseTest.l1.getWebElements("Category_Links","ShopNav\\CategoryLanding.properties").get(1).click();
		}
			
public void NavigateToSubcategory() throws Exception{
	BaseTest.l1.getWebElements("RootCategory_Links","ShopNav\\CategoryLanding.properties").get(1).click();
	BaseTest.l1.getWebElements("Category_Links","ShopNav\\CategoryLanding.properties").get(1).click();
	BaseTest.l1.getWebElements("Subcategory_Links","ShopNav\\CategoryLanding.properties").get(1).click();
	//li[@class='expandable fa fa-angle-right']/a
	}

public void NavigateToPDP() throws Exception{
	
	BaseTest.log.info("navigate to root category page");
	act.moveToElement(BaseTest.l1.getWebElement("RootCategory_Links","ShopNav\\CategoryLanding.properties")).perform();
	BaseTest.log.info("navigate to sub category page");
	BaseTest.l1.getWebElement("Subcategory_Links","ShopNav\\CategoryLanding.properties").click();
	Thread.sleep(2000);
	BaseTest.log.info("navigated to subCategory page and we should clcik on product name");
	BaseTest.l1.getWebElements("Category_NameLinks","ShopNav\\CategoryLanding.properties").get(3).click();
	Thread.sleep(2000);
	BaseTest.log.info("clicked on product name");
	
	}

public void SearchSuggestion(String product) throws Exception{
	
	BaseTest.l1.getWebElement("Search_TextBox","ShopNav\\SearchResults.properties").sendKeys(product);
	Thread.sleep(4000);
}

public void Search(String product) throws Exception{
	BaseTest.l1.getWebElement("Search_TextBox","ShopNav\\SearchResults.properties").click();
	//BaseTest.l1.getWebElement("Search_TextBox","ShopNav\\SearchResults.properties").clear();
	BaseTest.l1.getWebElement("Search_TextBox","ShopNav\\SearchResults.properties").sendKeys(product);
	Thread.sleep(4000);
	BaseTest.l1.getWebElement("Search_Find_Icon","ShopNav\\SearchResults.properties").click();
	Thread.sleep(4000);
}

public void SelectSwatch() throws Exception{
	

	
	try {
		if ((BaseTest.l1.getWebElement("pdp_Color_Label","ShopNav\\PDP.properties").isDisplayed())) {
			boolean ColorSwatchSelected=(BaseTest.l1.getWebElement("pdp_ColorSwatch_Selected","ShopNav\\PDP.properties")).isDisplayed();
			System.out.println(ColorSwatchSelected);
			if (ColorSwatchSelected==false)
			BaseTest.l1.getWebElements("Pdp_Color_swatch_Selectable","ShopNav\\PDP.properties").get(0).click();
			p.PDP_Pcolor=BaseTest.l1.getWebElement("PDP_Product_Color", "ShopNav\\PDP.properties").getText();
		}
	} catch (Exception e) {
		BaseTest.log.info("Color swatch is not present");
	}
	
	Thread.sleep(3000);
	
	try {
		if (BaseTest.l1.getWebElement("pdp_Size_Label","ShopNav\\PDP.properties").isDisplayed()){
			Thread.sleep(3000);
			boolean SizeSwatchSelected=(BaseTest.l1.getWebElement("pdp_SizeSwatch_Selected","ShopNav\\PDP.properties")).isDisplayed();
			System.out.println(SizeSwatchSelected);
			log.info(SizeSwatchSelected+"Colorswatch");
			if (SizeSwatchSelected==false)
				log.info("Click on Size swatch");
				BaseTest.l1.getWebElements("Pdp_Size_Swatch_Selectable","ShopNav\\PDP.properties").get(0).click();
			Thread.sleep(3000);
			p.PDP_Psize=BaseTest.l1.getWebElement("PDP_Product_Size", "ShopNav\\PDP.properties").getText();
		}
	} catch (Exception e) {
		BaseTest.log.info("PDP size is not present");
	}
	
	BaseTest.log.info("PDP Product Name");
	p.PDP_Pname=BaseTest.l1.getWebElement("PDP_Product_Name", "ShopNav\\PDP.properties").getText();
	BaseTest.log.info(p.PDP_Pname);
	Thread.sleep(3000);
	
	BaseTest.log.info(p.PDP_Pname);
	PDP_Price=BaseTest.l1.getWebElement("PDP_Product_Price", "ShopNav\\PDP.properties").getText();
	 
		
	}

public void AddtoCart() throws Exception{
	
	//Navigate to PDP
	NavigateToPDP();
	//Select Swatch
	SelectSwatch();
	BaseTest.log.info("PDP Add to cart button");
	BaseTest.l1.getWebElement("PDP_AddToCart","ShopNav\\PDP.properties").click();
	Thread.sleep(2000);
	BaseTest.l1.getWebElement("PDP_AddToCart","ShopNav\\PDP.properties").click();
	
	try {
		
		BaseTest.sa.assertTrue(gVar.assertVisible("PDP_bonus_product","ShopNav\\PDP.properties"),"PDP bonus product popup");
		BaseTest.l1.getWebElement("PDP_bonus_Nothanksbutton","ShopNav\\PDP.properties").click();
		
	} catch (Exception e) {
		
		BaseTest.log.info("POP up is not visible");

	}

}

public void Pagination() throws Exception{
	
	if(l1.getWebElement("CLP_Pagination_Div", "ShopNav\\CategoryLanding.properties").getText() == l1.getWebElement("CLP_ResultsHit", "ShopNav\\CategoryLanding.properties").getText())
		{
			System.out.println("Single page is displayed");		
		}
	else
		{
			if(l1.getWebElement("CLP_Pagination_PageNext", "ShopNav\\CategoryLanding.properties").isDisplayed() && l1.getWebElement("CLP_Pagination_PageLast", "ShopNav\\CategoryLanding.properties").isDisplayed())
				{	
					BaseTest.sa.assertTrue(gVar.assertVisible("CLP_Pagination_PageNext", "ShopNav\\CategoryLanding.properties"), "CLP_Pagination_PageNext display");
					BaseTest.sa.assertTrue(gVar.assertVisible("CLP_Pagination_PageLast", "ShopNav\\CategoryLanding.properties"), "CLP_Pagination_PageNext display");
					BaseTest.sa.assertTrue(gVar.assertNotVisible("CLP_Pagination_Page_Previous", "ShopNav\\CategoryLanding.properties"),"Page Previous link should not display");
					BaseTest.sa.assertTrue(gVar.assertNotVisible("CLP_Pagination_Page_First", "ShopNav\\CategoryLanding.properties"),"Page First link should not display");
					
					if(l1.getWebElement("CLP_Pagination_PageLast", "ShopNav\\CategoryLanding.properties").isDisplayed())
						{
						l1.getWebElement("CLP_Pagination_PageLast", "ShopNav\\CategoryLanding.properties").click();
						Thread.sleep(5000);
						}
					
					BaseTest.sa.assertTrue(gVar.assertVisible("CLP_Pagination_Page_Previous", "ShopNav\\CategoryLanding.properties"),"Page Previous link should not display");
					BaseTest.sa.assertTrue(gVar.assertVisible("CLP_Pagination_Page_First", "ShopNav\\CategoryLanding.properties"),"Page First link should not display");
					BaseTest.sa.assertTrue(gVar.assertNotVisible("CLP_Pagination_PageNext", "ShopNav\\CategoryLanding.properties"), "CLP_Pagination_PageNext display");
					BaseTest.sa.assertTrue(gVar.assertNotVisible("CLP_Pagination_PageLast", "ShopNav\\CategoryLanding.properties"), "CLP_Pagination_PageNext display");
					
					int pages= l1.getWebElement("CLP_Pagination_CurrentPage", "ShopNav\\CategoryLanding.properties").getText().length();
					
					if(l1.getWebElement("CLP_Pagination_Page_First", "ShopNav\\CategoryLanding.properties").isDisplayed())
						{
						l1.getWebElement("CLP_Pagination_Page_First", "ShopNav\\CategoryLanding.properties").click();
						Thread.sleep(5000);
						}				
					
					for(int j=1;j<=pages;j++)
						{
							if(j==1)
								{
									int curTop=l1.getWebElement("CLP_Pagination_CurrentPage", "ShopNav\\CategoryLanding.properties").getText().length();
						  	    	BaseTest.sa.assertEquals(j,curTop);
						  	    	int curBottom= l1.getWebElement("CLP_Pagination_CurrentPageFooter", "ShopNav\\CategoryLanding.properties").getText().length();
						  	    	BaseTest.sa.assertEquals(j,curBottom);
								}
							else
								{
									l1.getWebElement("CLP_Pagination_PageNext", "ShopNav\\CategoryLanding.properties").click();
									Thread.sleep(5000);
									int curTop=l1.getWebElement("CLP_Pagination_CurrentPage", "ShopNav\\CategoryLanding.properties").getText().length();
									BaseTest.sa.assertEquals(j,curTop);
						  	    	int curBottom= l1.getWebElement("CLP_Pagination_CurrentPageFooter", "ShopNav\\CategoryLanding.properties").getText().length();
						  	    	BaseTest.sa.assertEquals(j,curBottom);					
								}				
						}
					for(int j=pages;j>=1;j--)
						{
							if(j==pages)
								{
								int curTop=l1.getWebElement("CLP_Pagination_CurrentPage", "ShopNav\\CategoryLanding.properties").getText().length();
								BaseTest.sa.assertEquals(j,curTop);
					  	    	int curBottom= l1.getWebElement("CLP_Pagination_CurrentPageFooter", "ShopNav\\CategoryLanding.properties").getText().length();
					  	    	BaseTest.sa.assertEquals(j,curBottom);
								}
							else
								{
								l1.getWebElement("CLP_Pagination_Page_Previous", "ShopNav\\CategoryLanding.properties").click();
								Thread.sleep(5000);
								int curTop=l1.getWebElement("CLP_Pagination_CurrentPage", "ShopNav\\CategoryLanding.properties").getText().length();
								BaseTest.sa.assertEquals(j,curTop);
					  	    	int curBottom= l1.getWebElement("CLP_Pagination_CurrentPageFooter", "ShopNav\\CategoryLanding.properties").getText().length();
					  	    	BaseTest.sa.assertEquals(j,curBottom);
					  	    	
								}				
						}
					for(int j=1;j<=pages;j++)
						{
							if(j==1)
								{
								int curTop=l1.getWebElement("CLP_Pagination_CurrentPage", "ShopNav\\CategoryLanding.properties").getText().length();
								BaseTest.sa.assertEquals(j,curTop);
					  	    	int curBottom= l1.getWebElement("CLP_Pagination_CurrentPageFooter", "ShopNav\\CategoryLanding.properties").getText().length();
					  	    	BaseTest.sa.assertEquals(j,curBottom);
								}
							else
								{
									l1.getWebElements("CLP_Pagination_PageNo_Link", "ShopNav\\CategoryLanding.properties").get(j).click();
									Thread.sleep(5000);
									int curTop=l1.getWebElement("CLP_Pagination_CurrentPage", "ShopNav\\CategoryLanding.properties").getText().length();
									BaseTest.sa.assertEquals(j,curTop);
						  	    	int curBottom= l1.getWebElement("CLP_Pagination_CurrentPageFooter", "ShopNav\\CategoryLanding.properties").getText().length();
						  	    	BaseTest.sa.assertEquals(j,curBottom);							
								}				
						}
				 }
			else
				 {
					int pageDisp=l1.getWebElements("CLP_Pagination_PageNo_Display_Link", "ShopNav\\CategoryLanding.properties").size();
					for(int j=1;j<=pageDisp;j++)
						{
							if(j==1)
								{
								int curTop=l1.getWebElement("CLP_Pagination_CurrentPage", "ShopNav\\CategoryLanding.properties").getText().length();
								BaseTest.sa.assertEquals(j,curTop);
					  	    	int curBottom= l1.getWebElement("CLP_Pagination_CurrentPageFooter", "ShopNav\\CategoryLanding.properties").getText().length();
					  	    	BaseTest.sa.assertEquals(j,curBottom);
								}
							else
								{
								l1.getWebElements("CLP_Pagination_PageNo_Display_Link", "ShopNav\\CategoryLanding.properties").get(j).click();
								Thread.sleep(5000);
								int curTop=l1.getWebElement("CLP_Pagination_CurrentPage", "ShopNav\\CategoryLanding.properties").getText().length();
								BaseTest.sa.assertEquals(j,curTop);
					  	    	int curBottom= l1.getWebElement("CLP_Pagination_CurrentPageFooter", "ShopNav\\CategoryLanding.properties").getText().length();
					  	    	BaseTest.sa.assertEquals(j,curBottom);
					  	    	
								}				
						}				
				 }
			}
		}



	
}
